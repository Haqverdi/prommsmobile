import React from 'react';
import {
  createSwitchNavigator,
  createAppContainer,
  createDrawerNavigator,
  createStackNavigator,
} from 'react-navigation';

// mobx files
import { Provider } from 'mobx-react';
import { stores } from './stores/index';

// containers
import Home from './containers/Home/Home';
import SideBar from './containers/SideBar';
import Profile from './containers/Profile';
import Company from './containers/Company';
import Notify from './containers/Notify';
import Login from './containers/Login';
// auth containers
import SignIn from './containers/Auth/SignIn';
import SignUp from './containers/Auth/SignUp';
// subscreens
import CardInnerScreen from './containers/Home/homeStackscreens/CardInnerScreen';
import ProfileEdit from './containers/Profile/profileStackScreens/ProfileEdit';
import Create from './containers/Home/homeStackscreens/Create';

// main app
export default class App extends React.Component {
  render() {
    return (
      <Provider {...stores}>
        <AppContainer />
      </Provider>
    );
  }
}

// Drawer setup  with each route as Switch setup and them self as stack
const AppDrawerNavigator = createDrawerNavigator(
  {
    // screens
    Home: {
      screen: createSwitchNavigator({
        Home: createStackNavigator(
          {
            Home: {
              screen: Home,
            },
            CardInnerScreen: {
              screen: CardInnerScreen,
            },
            CreateScreen: {
              screen: Create,
            },
          },
          {
            headerMode: 'none',
          }
        ),
      }),
    },
    Company: {
      screen: createSwitchNavigator({
        screen: Company,
      }),
    },
    Profile: {
      screen: createSwitchNavigator({
        Profile: createStackNavigator(
          {
            Profile: {
              screen: Profile,
            },
            ProfileEdit: {
              screen: ProfileEdit,
            },
          },
          {
            headerMode: 'none',
          }
        ),
      }),
    },
    Notify: {
      screen: createSwitchNavigator({
        Notify: {
          screen: Notify,
        },
      }),
    },
  },
  // sidebar
  {
    contentComponent: SideBar,
    drawerType: 'front',
  }
);

// main switch navigation setup
const AppSwitchNavigator = createSwitchNavigator(
  {
    App: {
      screen: AppDrawerNavigator,
    },
    Login: {
      screen: Login,
    },
    SignIn: {
      screen: SignIn,
    },
    SignUp: {
      screen: SignUp,
    },
  },
  {
    initialRouteName: 'Login',
  }
);

// main navigation container
const AppContainer = createAppContainer(AppSwitchNavigator);
