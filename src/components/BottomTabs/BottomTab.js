/**
 * Bottom tab compnent(icon + title)
 */
import React, { PureComponent } from 'react';
import { StyleSheet, Text } from 'react-native';
import { Button } from 'native-base';
import { colors, fonts } from '../../theme';
import PropTypes from 'prop-types';

export default class BottomTab extends PureComponent {
  static propTypes = {
    active: PropTypes.bool,
    clicked: PropTypes.func,
    title: PropTypes.string,
  };

  render() {
    return (
      <Button vertical onPress={this.props.active ? null : this.props.clicked}>
        {this.props.children}
        <Text style={this.props.active ? styles.activeTabText : styles.tabText}>
          {this.props.title}
        </Text>
      </Button>
    );
  }
}

const styles = StyleSheet.create({
  activeTabText: {
    fontSize: 12,
    color: colors.main,
    fontFamily: fonts.PrivaProTwo,
  },
  tabText: {
    fontSize: 12,
    color: colors.muttedTextColor,
    fontFamily: fonts.PrivaProTwo,
  },
});
