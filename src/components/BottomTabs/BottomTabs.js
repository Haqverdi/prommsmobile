/**
 * Bottom Tabs for (custom) navigation (on button pressed call func
 * from TabNavigations to set selected component to root)
 */
import React, { PureComponent } from 'react';
import { StyleSheet } from 'react-native';
import { Footer, FooterTab, Button } from 'native-base';
import {
  colors,
  OrderCreateIcon,
  HomeIcon,
  CompanyIcon,
  NotifyIcon,
  ProfileIcon,
} from '../../theme';
import BottomTab from './BottomTab';
import { goToBottomTab, openOrderCreateModal } from '../../TabNavigations';

export default class BottomTabs extends PureComponent {
  render() {
    const { activeTab } = this.props;
    return (
      <Footer>
        <FooterTab style={styles.container}>
          {/* Home */}
          <BottomTab
            clicked={() => goToBottomTab('Home')}
            active={activeTab == 'home' ? true : false}
            title="Home"
          >
            <HomeIcon active={activeTab == 'home' ? true : false} />
          </BottomTab>
          {/* Home */}
          {/* Company */}
          <BottomTab
            clicked={() => goToBottomTab('Company')}
            active={activeTab == 'company' ? true : false}
            title="Company"
          >
            <CompanyIcon active={activeTab == 'company' ? true : false} />
          </BottomTab>
          {/* Company */}
          {/* ADD Button */}
          <Button
            vertical
            style={styles.plusTab}
            onPress={() => openOrderCreateModal()}
          >
            <OrderCreateIcon />
          </Button>
          {/* ADD Button */}
          {/* Notify */}
          <BottomTab
            // clicked={goToBottomTab('Notify')}
            active={activeTab == 'notify' ? true : false}
            title="Notify"
          >
            <NotifyIcon active={activeTab == 'notify' ? true : false} />
          </BottomTab>
          {/* Notify */}
          {/* Profile */}
          <BottomTab
            clicked={() => goToBottomTab('Profile')}
            active={activeTab == 'person' ? true : false}
            title="Profile"
          >
            <ProfileIcon active={activeTab == 'person' ? true : false} />
          </BottomTab>
          {/* Profile */}
        </FooterTab>
      </Footer>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    height: 56,
  },
  plusTab: {
    backgroundColor: colors.greyBackground,
    height: '100%',
  },
});
