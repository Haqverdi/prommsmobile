/**
 * Create page general info component, shows only data from props
 * used in /Containers/Create/Manual.js and QRcode.js
 */
import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import { colors, fonts } from '../../theme/index';

export const Generalinfo = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.generalInfoText}>
        {/* {'General Information'.toUpperCase()} */}
        GENERAL INFORMATION
      </Text>
      <Text style={styles.subtitle}>Reguested by</Text>
      <View style={styles.details}>
        <Text style={styles.detailsText}>Mammad Mammadov</Text>
        <Image
          style={styles.greyDot}
          source={require('../../assets/img/addElements/addPageGreyButton.png')}
        />
        <Text style={styles.detailsText}>#348732847287</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginBottom: 16,
    backgroundColor: colors.background,
    borderBottomColor: '#DCDCDC',
    borderBottomWidth: 1,
    paddingBottom: 12,
  },
  details: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: colors.background,
  },
  generalInfoText: {
    color: colors.main,
    fontFamily: fonts.PrivaProFour,
    fontSize: 14,
    lineHeight: 16,
  },
  subtitle: {
    fontFamily: fonts.PrivaProTwo,
    fontSize: 16,
    lineHeight: 24,
    color: 'rgba(0, 0, 0, 0.87)',
  },
  greyDot: {
    marginHorizontal: 8,
    width: 8,
    height: 8,
  },
  detailsText: {
    fontFamily: fonts.PrivaProOne,
    fontSize: 14,
    lineHeight: 20,
    color: 'rgba(0, 0, 0, 0.5438)',
  },
});
