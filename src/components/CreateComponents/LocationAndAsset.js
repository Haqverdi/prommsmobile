import React, { PureComponent } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { Button, Picker } from 'native-base';
import { colors, fonts } from '../../theme';

export class LocationAndAsset extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      activeButton: 'Location',
      // picker data
      Location: 'key1',
      Asset: 'key2',
    };
  }

  _handleButtonClick = button => {
    this.setState({
      activeButton: button,
    });
  };

  _handlePickerChange = (activeButton, value) => {
    this.setState({
      [activeButton]: value,
    });
  };

  render() {
    return (
      <View>
        <View style={styles.buttonsContainer}>
          <Button
            full
            style={[
              styles.button,
              this.state.activeButton == 'Asset' && styles.activeButton,
            ]}
            onPress={() => this._handleButtonClick('Asset')}
          >
            <Text
              style={[
                styles.buttonText,
                this.state.activeButton == 'Asset' && styles.buttonTextActive,
              ]}
            >
              Asset
            </Text>
          </Button>
          <Button
            full
            style={[
              styles.button,
              this.state.activeButton == 'Location' && styles.activeButton,
            ]}
            onPress={() => this._handleButtonClick('Location')}
          >
            <Text
              style={[
                styles.buttonText,
                this.state.activeButton == 'Location' &&
                  styles.buttonTextActive,
              ]}
            >
              Location
            </Text>
          </Button>
        </View>
        <View>
          {/* picker */}
          <View style={styles.orderTypeContainer}>
            <Text style={styles.orderTypeText}>{this.state.activeButton}</Text>
            <Picker
              selectedValue={this.state[this.state.activeButton]}
              style={styles.orderTypePicker}
              onValueChange={itemValue =>
                this._handlePickerChange(this.state.activeButton, itemValue)
              }
              headerBackButtonText="Geri"
              iosHeader={this.state.activeButton}
            >
              <Picker.Item label="Lorem Ipsum1" key="key1" value="key1" />
              <Picker.Item label="Lorem Ipsum2" key="key2" value="key2" />
              <Picker.Item label="Lorem Ipsum3" key="key3" value="key3" />
              <Picker.Item label="Lorem Ipsum4" key="key4" value="key4" />
              <Picker.Item label="Lorem Ipsum5" key="key5" value="key5" />
            </Picker>
          </View>
          {/* picker */}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  buttonsContainer: {
    flexDirection: 'row',
    width: '100%',
    height: 40,
    marginBottom: 16,
  },
  button: {
    width: '50%',
    height: 40,
    backgroundColor: '#ECECEC',
    elevation: 0,
  },
  activeButton: {
    backgroundColor: '#4FAD7E',
  },
  buttonText: {
    fontFamily: fonts.PrivaProTwo,
    fontSize: 14,
    lineHeight: 16,
    color: '#868686',
  },
  buttonTextActive: {
    color: '#FFFFFF',
  },
  // order type select styles
  orderTypeContainer: {
    marginBottom: 16,
  },
  orderTypePicker: {
    backgroundColor: colors.white,
    width: '100%',
    height: 40,
  },
  orderTypeText: {
    fontFamily: fonts.PrivaProTwo,
    fontSize: 12,
    lineHeight: 16,
    color: '#868686',
    marginBottom: 8,
  },
});
