/**
 * Create`s Manual page photo add component
 */
import React, { PureComponent } from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  Image,
  Platform,
} from 'react-native';
import { Button, Icon } from 'native-base';
import { Navigation } from 'react-native-navigation';
import { colors, fonts } from '../../theme/index';

export class PhotoAdd extends PureComponent {
  constructor(props) {
    super(props);
  }

  // show selected image in modal
  _showPhotoViewModal = (imageUrl, _handleImageDelete, index) => {
    Navigation.showModal({
      component: {
        id: 'PhotoShowModal',
        name: 'PhotoShowModal',
        passProps: {
          imageUrl: imageUrl,
          _handleImageDelete: _handleImageDelete,
          index: index,
        },
      },
    });
  };

  render() {
    const { _handleImagePicker, images, _handleImageDelete } = this.props;
    return (
      <View>
        <Text style={styles.addPhotoTitle}> Şəkilin əlavə edilməsi </Text>
        <FlatList
          horizontal
          showsHorizontalScrollIndicator={false}
          data={images}
          keyExtractor={(item, index) => item.name + index}
          renderItem={({ item, index }) => (
            <TouchableOpacity
              onPress={() =>
                this._showPhotoViewModal(
                  Platform.OS == 'ios' ? item.source : item.base64Data,
                  _handleImageDelete,
                  index
                )
              }
            >
              <Image
                style={styles.photo}
                source={{
                  uri: Platform.OS == 'ios' ? item.source : item.base64Data,
                }}
                key={item.name}
              />
            </TouchableOpacity>
          )}
        />
        <FlatList
          showsVerticalScrollIndicator={false}
          data={images}
          keyExtractor={(item, index) => item.name + index}
          style={{ marginTop: 16 }}
          renderItem={({ item }) => (
            <Text key={item.name} style={styles.imageNameText}>
              {item.name}{' '}
              <Text key={item.name} style={styles.imageSizeText}>
                {item.size} MB
              </Text>
            </Text>
          )}
        />
        <Button
          block
          style={styles.button}
          onPress={() => _handleImagePicker()}
        >
          <Icon
            style={{ color: 'white' }}
            name="camera-enhance"
            type="MaterialCommunityIcons"
          />
          <Text style={styles.buttonText}>Add Photo</Text>
        </Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  addPhotoTitle: {
    fontFamily: fonts.PrivaProTwo,
    fontSize: 12,
    lineHeight: 16,
    color: '#868686',
    marginBottom: 10,
  },
  imageNameText: {
    fontFamily: fonts.PrivaProTwo,
    fontSize: 14,
    lineHeight: 16,
    marginBottom: 12,
  },
  imageSizeText: {
    color: 'rgba(0,0,0,0.5)',
  },
  photo: {
    width: 120,
    height: 120,
    marginRight: 8,
  },
  // photo add button
  button: {
    backgroundColor: '#F5A623',
    marginBottom: 20,
    height: 40,
    borderRadius: 2,
  },
  buttonText: {
    fontFamily: fonts.PrivaProFour,
    fontSize: 16,
    // lineHeight: 16,
    fontWeight: 'bold',
    color: colors.white,
  },
});
