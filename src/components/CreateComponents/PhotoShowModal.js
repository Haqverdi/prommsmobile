/**
 * Photo show modal component, for create manual page,
 * for show added photo and delete
 */
import React, { PureComponent } from 'react';
import { Text, StyleSheet, Image, Modal } from 'react-native';
import { Container, Button, Icon } from 'native-base';
import { colors, fonts } from '../../theme';
import { SimpleHeader } from '../Header/SimpleHeader';

export class PhotoShowModal extends PureComponent {
  constructor(props) {
    super(props);
  }

  // navigations button pressed
  _closeModal = () => {};

  // delete image
  _imageDelete = () => {
    this.props._handleImageDelete(this.props.index);
    this._closeModal();
  };

  render() {
    const { imageUrl, photoShowModalVisible } = this.props;
    return (
      <Modal
        animationType="slide"
        onRequestClose={this._closeModal}
        transparent={false}
        visible={photoShowModalVisible}
      >
        <Container>
          {/* Custom header */}
          <SimpleHeader
            title="Photo View"
            navigationHandle={this._closeModal}
          />
          {/* Custom header end */}
          {/* Main container */}
          <Container style={styles.container}>
            <Image
              source={{ uri: imageUrl }}
              style={{
                width: '100%',
                height: '80%',
                resizeMode: 'cover',
                backgroundColor: 'grey',
              }}
            />
            <Button
              block
              style={styles.button}
              onPress={() => this._imageDelete()}
            >
              <Icon
                style={{ color: 'white' }}
                name="delete-forever"
                type="MaterialCommunityIcons"
              />
              <Text style={styles.buttonText}>Delete Photo</Text>
            </Button>
          </Container>
        </Container>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.background,
    paddingHorizontal: 16,
    paddingTop: 16,
  },
  button: {
    backgroundColor: '#D84C56',
    marginBottom: 20,
    marginTop: 24,
    height: 40,
    borderRadius: 2,
  },
  buttonText: {
    fontFamily: fonts.PrivaProFour,
    fontSize: 16,
    // lineHeight: 16,
    fontWeight: 'bold',
    color: colors.white,
  },
});
