/**
 * QR Code scan modal component, opnening from task create page
 */

import React, { PureComponent } from 'react';
import { Text, View, StyleSheet, Linking } from 'react-native';
import { Navigation } from 'react-native-navigation';
import { Container, Header, Title, Icon, Right, Left } from 'native-base';
import { colors, fonts } from '../../theme/index';
import QRCodeScanner from 'react-native-qrcode-scanner';
// import { RNCamera } from 'react-native-camera';

export class QRCodeScanModal extends PureComponent {
  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
  }

  //rnn options
  static options() {
    return {
      topBar: {
        visible: false,
        height: 0,
      },
    };
  }

  // navigations button pressed
  _closeModal = () => {
    Navigation.dismissModal(this.props.componentId);
  };

  // on qrcode scanned
  onSuccess(e) {
    Linking.openURL(e.data).catch(err =>
      console.error('An error occured', err)
    );
  }

  render() {
    return (
      <Container>
        {/* Custom header */}
        <Header style={{ backgroundColor: colors.main }}>
          {/* left title */}
          <Left>
            <Title
              style={{
                color: colors.white,
                fontFamily: fonts.PrivaProThree,
                fontSize: 18,
                lineHeight: 24,
                marginLeft: 8,
                width: '100%',
                textAlign: 'left',
              }}
            >
              Add QR Code
            </Title>
          </Left>
          {/* left title */}
          <Right>
            {/* close btn */}
            <Icon
              type="MaterialCommunityIcons"
              name="close"
              style={{ color: '#FFFFFF' }}
              onPress={this._closeModal}
            />
            {/* close btn */}
          </Right>
        </Header>
        {/* Custom header */}
        <Container style={styles.container}>
          <View style={styles.textContainer}>
            <Text style={styles.text} numberOfLines={4}>
              Lorem ipsum dolor sit amet, ea his nobis iisque tractatos.
              Suscipit efficiendi disputando id vix, et unum nihil nominati pro.
              Ei nec bonorum mandamus.
            </Text>
          </View>
          <View style={styles.imageContainer}>
            {/* <QRCodeScanner
              onRead={this.onSuccess.bind(this)}
              topContent={null}
              bottomContent={null}
            /> */}
          </View>
        </Container>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.background,
    paddingHorizontal: 16,
    paddingTop: 45,
  },
  textContainer: {
    marginBottom: 44,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    textAlign: 'center',
    fontFamily: fonts.PrivaProOne,
    fontSize: 14,
    lineHeight: 20,
    color: '#373737',
  },
  imageContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
});
