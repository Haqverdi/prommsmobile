import { Generalinfo } from './Generalinfo';
import { LocationAndAsset } from './LocationAndAsset';
import { QRCodeScanModal } from './QRCodeScanModal';
import { PhotoAdd } from './PhotoAdd';
import { PhotoShowModal } from './PhotoShowModal';

export {
  Generalinfo,
  LocationAndAsset,
  QRCodeScanModal,
  PhotoAdd,
  PhotoShowModal,
};
