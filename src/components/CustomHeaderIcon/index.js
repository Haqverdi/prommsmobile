/**
 * custom header icon
 */
import React from 'react';
import { Icon } from 'native-base';

const CustomHeaderIcon = ({ name, onPress, ...props }) => {
  return (
    <Icon
      name={name}
      type="MaterialCommunityIcons"
      style={{ color: '#FFFFFF' }}
      onPress={onPress}
      {...props}
    />
  );
};

export default CustomHeaderIcon;
