/*
  Header component with search and filter button
*/
import React, { PureComponent } from 'react';
import { StyleSheet, TouchableOpacity, Platform, Text } from 'react-native';
import { Header, Body, Left, Right, Icon } from 'native-base';
import PropTypes from 'prop-types';
import { colors, fonts } from '../../theme';

// platform check
const platform = Platform.OS;

export default class MyHeader extends PureComponent {
  static propTypes = {
    showSearchButton: PropTypes.bool,
    showSettingsButton: PropTypes.bool,
    title: PropTypes.string,
    navigation: PropTypes.object,
    rightButtonPressHandle: PropTypes.func,
  };

  render() {
    const {
      showSearchButton,
      showSettingsButton,
      title,
      navigation,
      rightButtonPressHandle,
    } = this.props;
    return (
      <Header style={headerStyles.header}>
        {/* Left icon */}
        <Left style={headerStyles.left}>
          <TouchableOpacity
            onPress={() => navigation.toggleDrawer()}
            style={headerStyles.leftOpacity}
          >
            <Icon
              style={headerStyles.icon}
              name="menu"
              type="MaterialCommunityIcons"
            />
          </TouchableOpacity>
        </Left>
        {/* Center */}
        <Body style={headerStyles.body}>
          {/* title */}
          <Text style={headerStyles.bodyText}>{title}</Text>
          {/* search icon */}
          {showSearchButton && (
            <TouchableOpacity style={headerStyles.bodyOpacity}>
              <Icon
                name="magnify"
                type="MaterialCommunityIcons"
                style={headerStyles.icon}
              />
            </TouchableOpacity>
          )}
        </Body>
        {/* Right icon */}
        <Right style={headerStyles.right}>
          {showSettingsButton ? (
            <TouchableOpacity
              onPress={() => rightButtonPressHandle()}
              style={headerStyles.rightOpacity}
            >
              <Icon
                name="dots-vertical"
                type="MaterialCommunityIcons"
                style={headerStyles.icon}
              />
            </TouchableOpacity>
          ) : null}
        </Right>
      </Header>
    );
  }
}

// styles
const headerStyles = StyleSheet.create({
  header: {
    backgroundColor: colors.main,
    paddingLeft: platform == 'ios' ? 10 : null,
    elevation: 0,
  },
  left: {
    flex: 1,
  },
  leftOpacity: {
    width: '100%',
    alignItems: platform == 'android' ? 'center' : 'flex-start',
  },
  body: {
    justifyContent: 'space-between',
    alignItems: 'center',
    alignContent: 'center',
    flexDirection: 'row',
    flex: 5,
  },
  bodyText: {
    color: colors.white,
    fontSize: 20,
    lineHeight: 24,
    marginLeft: 10,
    fontFamily: fonts.RobotoRegular,
  },
  bodyOpacity: {
    width: '30%',
    alignItems: 'flex-end',
  },
  right: {
    flex: 1,
    alignItems: 'flex-end',
  },
  rightOpacity: {
    width: '100%',
    alignItems: 'flex-end',
  },
  icon: { color: colors.white },
});
