/**
 * simple header button
 */

import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { Header, Left, Icon, Title, Right } from 'native-base';
import { colors, fonts } from '../../theme/index';

export const SimpleHeader = ({
  navigationHandle,
  rightButtonPressHandle,
  title,
  hasTabs,
  showRightButton,
}) => {
  return (
    <Header style={SimpleHeaderStyles.header} hasTabs={hasTabs}>
      <Left style={SimpleHeaderStyles.left}>
        <TouchableOpacity onPress={() => navigationHandle()}>
          <Icon
            name="close"
            type="MaterialCommunityIcons"
            style={{ color: '#FFFFFF' }}
          />
        </TouchableOpacity>
        <Title style={SimpleHeaderStyles.title}>{title}</Title>
      </Left>
      {showRightButton && (
        <Right>
          <Icon
            name="check"
            type="MaterialCommunityIcons"
            style={{ color: colors.white }}
            onPress={() => rightButtonPressHandle()}
          />
        </Right>
      )}
    </Header>
  );
};

// proptypes
SimpleHeader.propTypes = {
  title: PropTypes.string,
  navigationHandle: PropTypes.func,
  hasTabs: PropTypes.bool,
  rightButtonPressHandle: PropTypes.func,
};

// styles
const SimpleHeaderStyles = StyleSheet.create({
  header: {
    backgroundColor: colors.main,
    justifyContent: 'flex-start',
  },
  left: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
  },
  title: {
    fontSize: 20,
    fontFamily: fonts.RobotoRegular,
    fontWeight: 'bold',
    lineHeight: 24,
    color: colors.white,
    marginLeft: 37,
  },
});
