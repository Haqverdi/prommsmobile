/**
 * Order modal bottom buttons
 */
import React, { PureComponent } from 'react';
import { StyleSheet, Text } from 'react-native';
import { Footer, FooterTab, Button } from 'native-base';
import { colors, fonts } from '../../theme';
import PropTypes from 'prop-types';

export class BottomButtons extends PureComponent {
  static propTypes = {
    withTwoButtons: PropTypes.bool,
  };

  render() {
    const { withTwoButtons, buttonsPressHandle } = this.props;
    return (
      <Footer>
        <FooterTab style={buttonsStyles.container}>
          {/* Start */}
          <Button
            full
            style={buttonsStyles.confirmButton}
            onPress={() => buttonsPressHandle('first')}
          >
            <Text style={buttonsStyles.buttonText}>Accept</Text>
          </Button>
          {/* Start end */}
          {/* Cancel */}
          <Button
            full
            style={buttonsStyles.startButton}
            onPress={() => buttonsPressHandle('second')}
          >
            <Text style={buttonsStyles.buttonText}>Reject</Text>
          </Button>
          {/* Cancel end */}
          {/* Confirm */}
          {/* check for 3 or 2 buttons view */}
          {!withTwoButtons && (
            <Button
              full
              style={buttonsStyles.cancelButton}
              onPress={() => buttonsPressHandle('third')}
            >
              <Text style={buttonsStyles.buttonText}>Cancel</Text>
            </Button>
          )}
          {/* Confirm end */}
        </FooterTab>
      </Footer>
    );
  }
}

const buttonsStyles = StyleSheet.create({
  container: {
    height: 56,
  },
  startButton: {
    backgroundColor: '#FAB900',
  },
  cancelButton: {
    backgroundColor: '#DE505A',
  },
  confirmButton: {
    backgroundColor: colors.main,
  },
  buttonText: {
    color: colors.white,
    fontFamily: fonts.RobotoRegular,
    fontStyle: 'normal',
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 24,
    textAlign: 'center',
  },
});
