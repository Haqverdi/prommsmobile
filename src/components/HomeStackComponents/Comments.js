import React, { PureComponent } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Keyboard,
  SafeAreaView,
} from 'react-native';
import { colors, fonts } from '../../theme';
import PropTypes from 'prop-types';
import { Input } from 'native-base';
import { GiftedChat, Bubble, Send } from 'react-native-gifted-chat';

class Comments extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      messages: [
        {
          _id: 'id1',
          text: 'Hello developer',
          createdAt: new Date(),
          user: {
            _id: 'id2',
            name: 'React Native',
          },
        },
      ],
      inputValue: '',
    };
  }

  static propTypes = {};

  _handleInputChange = text => {
    this.setState({
      inputValue: text,
    });
  };

  // _handleSmileClick = () => {
  //   Keyboard.
  // }

  renderBubble = props => {
    return (
      <Bubble
        {...props}
        wrapperStyle={{
          right: {
            backgroundColor: colors.main,
          },
        }}
      />
    );
  };

  renderAction = () => {
    return (
      <TouchableOpacity
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          height: '100%',
          marginLeft: 16,
        }}
      >
        <Image source={require('../../assets/img/plus.png')} />
      </TouchableOpacity>
    );
  };

  renderSend = props => {
    return (
      <Send {...props}>
        <TouchableOpacity
          style={{
            height: '100%',
            justifyContent: 'center',
            marginRight: 16,
          }}
        >
          {this.state.inputValue.length > 0 ? (
            <Image source={require('../../assets/img/right-arrow.png')} />
          ) : (
            <Image source={require('../../assets/img/smile.png')} />
          )}
        </TouchableOpacity>
      </Send>
    );
  };

  onSend = (messages = []) => {
    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, messages),
    }));
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <GiftedChat
          messages={this.state.messages}
          onSend={messages => this.onSend(messages)}
          user={{
            _id: 'id1',
          }}
          onInputTextChanged={text => this._handleInputChange(text)}
          renderBubble={this.renderBubble}
          locale="az"
          // renderAvatar={null}
          // renderActions={this.renderAction}
          // renderSend={this.renderSend}
        />
      </SafeAreaView>
    );
  }
}

const InputArea = ({ inputValue, _handleInputChange, onSend }) => {
  return (
    <View style={styles.messageBox}>
      <TouchableOpacity>
        <Image source={require('../../assets/img/plus.png')} />
      </TouchableOpacity>
      <Input
        style={{ marginHorizontal: 8 }}
        placeholder="Type message"
        onChangeText={text => _handleInputChange(text)}
      />
      <TouchableOpacity onPress={() => onSend()}>
        {inputValue.length > 0 ? (
          <Image source={require('../../assets/img/right-arrow.png')} />
        ) : (
          <Image source={require('../../assets/img/smile.png')} />
        )}
      </TouchableOpacity>
    </View>
  );
};

const MyMessage = () => {
  return (
    <View style={styles.myMessage}>
      <Text>dsknskndvknsdkvnslv</Text>
      <Text>12:35</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.background,
    flex: 1,
  },
  messageBox: {
    backgroundColor: colors.white,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
    left: 0,
    height: 56,
    width: '100%',
    paddingHorizontal: 18,
    paddingVertical: 20,
  },
  myMessage: {
    maxWidth: '50%',
    width: '50%',
    minHeight: 38,
    backgroundColor: colors.main,
    position: 'absolute',
    right: 0,
  },
  otherMessage: {},
});

export default Comments;
