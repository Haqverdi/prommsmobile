import React, { PureComponent } from 'react';
import { Container, Content } from 'native-base';
import { InfoCard } from '../ProfileComponents/InfoCard';
import Photos from './Photos';
import { colors } from '../../theme';
import PropTypes from 'prop-types';

class Details extends PureComponent {
  constructor(props) {
    super(props);
  }
  static propTypes = {};
  render() {
    return (
      <Container style={{ backgroundColor: colors.background }}>
        <Content contentContainerStyle={{ paddingLeft: 14 }}>
          {/* info cards */}
          <InfoCard title="Test" info="some info" />
          <InfoCard title="Test" info="some info" />
          <InfoCard title="Test" info="some info" />
          <InfoCard title="Test" info="some info" />
          <InfoCard title="Test" info="some info" />
          <InfoCard title="Test" info="some info" />
          {/* Photos */}
          <Photos />
        </Content>
      </Container>
    );
  }
}

export default Details;
