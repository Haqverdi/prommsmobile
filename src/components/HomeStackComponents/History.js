import React, { PureComponent } from 'react';
import { StyleSheet } from 'react-native';
import { Container, Content } from 'native-base';
import { colors, fonts } from '../../theme';
import { TimelineCard, TimeShow } from '../TimelineComponents';

class History extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const data = [
      {
        title: 'Created',
        name: 'Mammad Mammadov',
        time: '25 may, 15:01:32',
        status: 'supervised',
      },
      {
        title: 'Created',
        name: 'Mammad Mammadov',
        time: '25 may, 15:01:32',
        status: 'paused',
      },
      {
        title: 'Created',
        name: 'Mammad Mammadov',
        time: '25 may, 15:01:32',
        status: 'closed',
      },
      {
        title: 'Created',
        name: 'Mammad Mammadov',
        time: '25 may, 15:01:32',
        status: 'paused',
      },
      {
        title: 'Created',
        name: 'Mammad Mammadov',
        time: '25 may, 15:01:32',
        status: 'rejected',
      },
    ];
    return (
      <Container style={styles.container}>
        <Content contentContainerStyle={styles.content}>
          <TimeShow position="start" time="2019" />
          {data.map((item, i, arr) => {
            return (
              <TimelineCard
                key={i}
                last={arr.length - 1 === i ? true : false}
                data={{ ...item }}
              />
            );
          })}
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.background,
  },
  content: {
    padding: 16,
  },
});

export default History;
