/**
 * Card modal `Details` part images viewer
 */

import React, { PureComponent } from 'react';
import { View, Modal } from 'react-native';
import PropTypes from 'prop-types';
import ImageViewer from 'react-native-image-zoom-viewer';

class Photo extends PureComponent {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    modalVisible: PropTypes.bool,
    images: PropTypes.array,
    handleModalClick: PropTypes.func,
  };

  componentWillUnmount = () => {
    this.props = null;
  };
  render() {
    const { modalVisible, handleModalClick, images } = this.props;
    return (
      <View>
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => handleModalClick()}
        >
          <ImageViewer
            imageUrls={images}
            backgroundColor="rgba(52, 52, 52, 0.8)"
            onSwipeDown={() => handleModalClick()}
            enableSwipeDown={true}
          />
        </Modal>
      </View>
    );
  }
}

export default Photo;
