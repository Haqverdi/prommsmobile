import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  StyleSheet,
  Image,
} from 'react-native';
import { colors, fonts } from '../../theme';
import Photo from './Photo';
class Photos extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      modalData: null,
    };
  }

  handleModalClick = data => {
    this.setState({
      modalVisible: !this.state.modalVisible,
      modalData: data,
    });
  };

  handleModalClose = () => {
    this.setState({
      modalVisible: false,
    });
  };

  render() {
    const data = [
      {
        key: 'Draft',
        url: 'https://placeimg.com/640/480/any',
      },
      {
        key: 'New',
        url: 'https://placeimg.com/640/480/any',
      },
      {
        key: 'In Progress',
        url: 'https://placeimg.com/640/480/any',
      },
      {
        key: 'Need Action',
        url: 'https://placeimg.com/640/480/any',
      },
      {
        key: 'John',
        url: 'https://placeimg.com/640/480/any',
      },
      {
        key: 'Jillian',
        url: 'https://placeimg.com/640/480/any',
      },
      {
        key: 'Jimmy',
        url: 'https://placeimg.com/640/480/any',
      },
    ];
    return (
      <View style={styles.tabList}>
        <Text style={styles.text}>Attachment</Text>
        <FlatList
          horizontal
          showsHorizontalScrollIndicator={false}
          data={data}
          renderItem={({ item }) => (
            <TouchableOpacity onPress={() => this.handleModalClick(item.url)}>
              <Image
                source={{ uri: item.url }}
                style={styles.itemGeneral}
                key={item.key}
              />
            </TouchableOpacity>
          )}
        />
        <Photo
          handleModalClick={() => this.handleModalClose()}
          modalData={this.state.modalData}
          modalVisible={this.state.modalVisible}
          images={data}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  tabList: {
    height: 120,
    marginBottom: 9,
  },
  itemGeneral: {
    height: 120,
    width: 120,
    marginRight: 16,
    marginTop: 6,
    backgroundColor: 'grey',
  },
  text: {
    fontFamily: fonts.PrivaProThree,
    fontSize: 14,
    color: 'rgba(0, 0, 0, 0.87)',
  },
});

export default Photos;
