// import React, { PureComponent } from 'react';
// import { StyleSheet } from 'react-native';
// import {
//   Tab,
//   Tabs,
//   TabHeading,
//   Text,
//   View,
//   Content,
//   Button,
//   ScrollableTab,
//   Container,
// } from 'native-base';
// import { colors } from '../../theme';
// import MyCard from '../card/index';

// export default class SecondTabs extends PureComponent {
//   render() {
//     return (
//       <Container>
//         <Content
//           padder
//           contentContainerStyle={{ backgroundColor: colors.background }}
//         >
//           <Tabs
//             locked
//             renderTabBar={() => (
//               <ScrollableTab
//                 style={{
//                   backgroundColor: colors.background,
//                   height: 24,
//                   borderBottomWidth: 0,
//                   marginTop: 12,
//                   justifyContent: 'flex-start',
//                   paddingLeft: 0,
//                 }}
//               />
//             )}
//             tabBarUnderlineStyle={{
//               width: 0,
//               elevation: 0,
//             }}
//           >
//             <Tab
//               tabStyle={{ justifyContent: 'flex-start', paddingLeft: 0 }}
//               activeTabStyle={{ justifyContent: 'flex-start', paddingLeft: 0 }}
//               heading={
//                 <TabHeading style={styles.heading}>
//                   <Text style={styles.headingText}>Draft</Text>
//                 </TabHeading>
//               }
//             >
//               <Container>
//                 <Content
//                   contentContainerStyle={{
//                     backgroundColor: colors.background,
//                     flex: 1,
//                     paddingTop: 12,
//                   }}
//                 >
//                   <MyCard icon="done" />
//                   <MyCard icon="timer" />
//                   {/* <MyCard icon="reject" />
//                   <MyCard icon="paused" />
//                   <MyCard icon="timer" /> */}
//                 </Content>
//               </Container>
//             </Tab>
//             <Tab
//               heading={
//                 <TabHeading style={styles.heading}>
//                   <Text style={styles.headingButton}>Draft</Text>
//                 </TabHeading>
//               }
//             >
//               <Container>
//                 <Content
//                   contentContainerStyle={{
//                     backgroundColor: colors.background,
//                     flex: 1,
//                   }}
//                 >
//                   <MyCard icon="done" />
//                   <MyCard icon="timer" />
//                   {/* <MyCard icon="reject" />
//                   <MyCard icon="paused" />
//                   <MyCard icon="timer" /> */}
//                 </Content>
//               </Container>
//             </Tab>
//             <Tab
//               heading={
//                 <TabHeading style={styles.heading}>
//                   <Text style={styles.headingButton}>Draft</Text>
//                 </TabHeading>
//               }
//             >
//               <Container>
//                 <Content
//                   contentContainerStyle={{
//                     backgroundColor: colors.background,
//                     flex: 1,
//                   }}
//                 >
//                   <MyCard icon="done" />
//                   <MyCard icon="timer" />
//                   {/* <MyCard icon="reject" />
//                   <MyCard icon="paused" />
//                   <MyCard icon="timer" /> */}
//                 </Content>
//               </Container>
//             </Tab>
//           </Tabs>
//         </Content>
//       </Container>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   heading: {
//     backgroundColor: colors.white,
//     borderRadius: 30,
//     borderColor: 'red',
//     borderWidth: 1,
//     minWidth: 50,
//     marginHorizontal: 0,
//     paddingHorizontal: 0,
//   },
//   headingText: {
//     color: colors.orange,
//     paddingHorizontal: 0,
//     marginHorizontal: 0,
//   },
// });
