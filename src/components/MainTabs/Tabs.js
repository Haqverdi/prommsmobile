import React, { PureComponent } from 'react';
import { StyleSheet } from 'react-native';
import { Tab, Tabs, Text, View } from 'native-base';
import { colors, fonts } from '../../theme';
import STabs from './exprements';

export default class MyTabs extends PureComponent {
  render() {
    const { onTabChange, _swipeButtonsPressHandle } = this.props;
    return (
      <Tabs
        tabBarUnderlineStyle={{ width: 0 }}
        tabBarActiveTextColor={colors.white}
        onChangeTab={({ i }) => onTabChange(i)}
        locked
      >
        <Tab
          tabStyle={styles.tabStyle}
          activeTabStyle={styles.activeTabStyle}
          activeTextStyle={styles.activeTextStyle}
          textStyle={styles.textStyle}
          heading="Hamisi"
        >
          <STabs
            _swipeButtonsPressHandle={_swipeButtonsPressHandle}
            onCardClick={id => this.props.onCardClick(id)}
          />
        </Tab>
        <Tab
          tabStyle={styles.tabStyle}
          activeTabStyle={styles.activeTabStyle}
          activeTextStyle={styles.activeTextStyle}
          textStyle={styles.textStyle}
          heading="Open"
        >
          <STabs
            _swipeButtonsPressHandle={_swipeButtonsPressHandle}
            onCardClick={id => this.props.onCardClick(id)}
          />
        </Tab>
        <Tab
          tabStyle={styles.tabStyle}
          activeTabStyle={styles.activeTabStyle}
          activeTextStyle={styles.activeTextStyle}
          textStyle={styles.textStyle}
          heading="Closed"
        >
          <View>
            <Text>Tab1</Text>
          </View>
        </Tab>
        <Tab
          tabStyle={styles.tabStyle}
          activeTabStyle={styles.activeTabStyle}
          activeTextStyle={styles.activeTextStyle}
          textStyle={styles.textStyle}
          heading="Cancel"
        >
          <View>
            <Text>Tab1</Text>
          </View>
        </Tab>
      </Tabs>
    );
  }
}

const styles = StyleSheet.create({
  tabStyle: { backgroundColor: colors.main },
  activeTabStyle: { backgroundColor: colors.main },
  activeTextStyle: {
    color: colors.white,
    fontFamily: fonts.RobotoMedium,
    fontSize: 14,
    lineHeight: 20,
  },
  textStyle: {
    color: colors.white,
    opacity: 0.5,
    fontFamily: fonts.RobotoMedium,
    fontSize: 14,
    lineHeight: 20,
  },
});
