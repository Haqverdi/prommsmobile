import React, { PureComponent } from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  Platform,
} from 'react-native';
import { Container, Content } from 'native-base';
import { colors, fonts } from '../../theme';
import MyCard from '../../components/Card';
import ListItem from '../Card/test';

export default class STabs extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: 'Draft',
    };
  }

  handleFilterChange = item => {
    this.setState({
      activeTab: item.key,
    });
  };

  render() {
    const { activeTab } = this.state;
    const { onCardClick, _swipeButtonsPressHandle } = this.props;
    return (
      <Container style={{ backgroundColor: colors.background }}>
        <Content padder showsVerticalScrollIndicator={false}>
          <View style={styles.tabList}>
            <FlatList
              horizontal
              showsHorizontalScrollIndicator={false}
              data={[
                { key: 'Draft' },
                { key: 'New' },
                { key: 'In Progress' },
                { key: 'Need Action' },
                { key: 'John' },
                { key: 'Jillian' },
                { key: 'Jimmy' },
              ]}
              renderItem={({ item }) => (
                <TouchableOpacity onPress={() => this.handleFilterChange(item)}>
                  <Text
                    style={
                      item.key == activeTab
                        ? [styles.itemGeneral, styles.itemActive]
                        : [styles.itemGeneral, styles.itemNonActive]
                    }
                    key={item.key}
                  >
                    {item.key}
                  </Text>
                </TouchableOpacity>
              )}
            />
          </View>
          <FlatList
            showsVerticalScrollIndicator={false}
            data={['1', '2', '3', '4', '5', '6']}
            keyExtractor={item => item}
            renderItem={({ item }) => (
              <MyCard
                icon="paused"
                onCardClick={() => onCardClick(item)}
                key={item}
                _swipeButtonsPressHandle={_swipeButtonsPressHandle}
              />
            )}
          />
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  tabList: {
    paddingBottom: 12,
    paddingTop: 2,
  },
  itemGeneral: {
    fontSize: 14,
    paddingHorizontal: 12,
    // paddingTop: Platform.OS === 'ios' ? 14 : 9,
    paddingTop: 9,
    fontStyle: 'normal',
    marginRight: 8,
    height: 40,
    borderRadius: 2,
    backgroundColor: colors.white,
    fontFamily: fonts.RobotoMedium,
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
  },
  itemNonActive: {
    color: colors.secondTabGreyText,
  },
  itemActive: {
    color: colors.orange,
  },
  separatorViewStyle: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  separatorStyle: {
    height: 1,
    backgroundColor: '#000',
  },
});
