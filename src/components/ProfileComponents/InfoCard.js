import React, { PureComponent } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { colors, fonts } from '../../theme';
import PropTypes from 'prop-types';

export class InfoCard extends PureComponent {
  static propTypes = {
    title: PropTypes.string,
    info: PropTypes.string,
  };
  render() {
    const { title, info } = this.props;
    return (
      <View style={styles.container}>
        <View>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.subTitle}>{info}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 75,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: colors.background,
    padding: 18,
  },
  title: {
    fontFamily: fonts.PrivaProThree,
    fontSize: 16,
    paddingBottom: 5,
    color: 'rgba(0, 0, 0, 0.87)',
  },
  subTitle: {
    fontFamily: fonts.PrivaProOne,
    fontSize: 14,
    color: 'rgba(0, 0, 0, 0.5438)',
  },
});
