import React, { PureComponent } from 'react';
import {
  View,
  ScrollView,
  Text,
  StyleSheet,
  Dimensions,
  TouchableWithoutFeedback,
  Platform,
} from 'react-native';
import { Header, Body, Right, Thumbnail } from 'native-base';
import Dialog, {
  DialogContent,
  SlideAnimation,
} from 'react-native-popup-dialog';
import { colors, fonts, IconGenerator } from '../../theme';

export class Popup extends PureComponent {
  constructor(props) {
    super(props);
  }

  componentDidMount = () => {
    this.props = null;
  };

  render() {
    const {
      modalVisible,
      modalTitle,
      modalData,
      handleModalClick,
    } = this.props;
    return (
      <View>
        <Dialog
          visible={modalVisible}
          onTouchOutside={() => handleModalClick()}
          dialogAnimation={
            new SlideAnimation({
              toValue: 0,
              slideFrom: 'bottom',
              useNativeDriver: true,
            })
          }
          dialogStyle={{
            borderRadius: 0,
            width: Dimensions.get('window').width - 32,
            maxHeight: Dimensions.get('window').height - 96,
          }}
        >
          <DialogContent style={styles.content}>
            <ScrollView
              style={{ width: '100%' }}
              showsVerticalScrollIndicator={false}
            >
              <Header
                style={{
                  backgroundColor: colors.main,
                  paddingTop: 0,
                }}
              >
                <Body>
                  <Text
                    style={{
                      justifyContent: 'flex-start',
                      width: '100%',
                      color: '#FFFFFF',
                      fontFamily: fonts.PrivaProThree,
                      fontSize: 18,
                      marginLeft: Platform.OS == 'ios' ? 16 : 8,
                    }}
                  >
                    {modalTitle}
                  </Text>
                </Body>
              </Header>
              {modalData.map((item, index) => (
                <SimpleCard key={item.title + index} {...item} />
              ))}
              {/* close btn */}
              <Header
                style={{
                  backgroundColor: colors.white,
                  paddingTop: 0,
                }}
              >
                <TouchableWithoutFeedback onPress={() => handleModalClick()}>
                  <Right>
                    <Text
                      style={{
                        color: colors.black,
                        fontFamily: fonts.PrivaProThree,
                        fontSize: 18,
                        marginRight: 10.5,
                      }}
                    >
                      Close
                    </Text>
                  </Right>
                </TouchableWithoutFeedback>
              </Header>
            </ScrollView>
          </DialogContent>
        </Dialog>
      </View>
    );
  }
}

const SimpleCard = ({ title, logo, count, type }) => {
  let typeColor;
  switch (type) {
    case 'supervised':
      typeColor = '#4C91E2';
      break;
    case 'paused':
      typeColor = '#AFAFAF';
      break;
    case 'rejected':
      typeColor = '#F5A623';
      break;
    case 'expired':
      typeColor = '#D33F49';
      break;
    case 'closed':
      typeColor = '#D33F49';
      break;
    case 'assigned':
      typeColor = '#4A90E2';
      break;
    default:
      typeColor = colors.black;
      break;
  }
  return (
    <Header
      style={{
        backgroundColor: colors.white,
        paddingTop: 0,
      }}
    >
      <Body
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          paddingLeft: 8,
        }}
      >
        {count != null && count != undefined ? (
          <IconGenerator name={type} width={40} height={40} />
        ) : (
          <Thumbnail
            source={{
              uri: logo,
            }}
            style={{
              width: 40,
              height: 40,
              padding: 0,
              margin: 0,
            }}
          />
        )}
        <View
          style={{
            width: '82%',
            height: '100%',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            marginRight: 16,
          }}
        >
          <Text
            style={{
              color: colors.black,
              fontFamily: fonts.PrivaProThree,
              fontSize: 18,
              marginLeft: 16,
            }}
          >
            {title}
          </Text>

          {count != null && count != undefined && (
            <Text
              style={{
                color: typeColor,
                fontFamily: fonts.PrivaProThree,
                fontSize: 18,
              }}
            >
              {count}
            </Text>
          )}
        </View>
      </Body>
    </Header>
  );
};

const styles = StyleSheet.create({
  content: {
    paddingLeft: 0,
    paddingRight: 0,
    paddingBottom: 0,
  },
});
