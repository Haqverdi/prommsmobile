import React, { PureComponent } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import PropTypes from 'prop-types';
import { colors, fonts } from '../../theme';

export default class ProfileCard extends PureComponent {
  constructor(props) {
    super(props);
  }
  static propTypes = {
    name: PropTypes.string,
    profilePicture: PropTypes.string,
    data: PropTypes.array,
  };
  render() {
    const { name, data } = this.props;
    return (
      <View style={cardStyles.body}>
        {/* picture part */}
        <View>
          <TouchableOpacity>
            <Image
              source={require('../../assets/img/profile/person.png')}
              style={cardStyles.profileImage}
            />
            <Image
              style={cardStyles.overlay}
              source={require('../../assets/img/profile/addPhoto.png')}
            />
          </TouchableOpacity>
          <Text style={cardStyles.text}>{name}</Text>
        </View>
        {/* picture part */}
        {/* bootom part */}
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-around',
            width: '100%',
          }}
        >
          <TouchableOpacity
            style={cardStyles.boxContainer}
            onPress={() => this.props.onPressed(data[0].title)}
          >
            <Text style={[cardStyles.boxNumber, { color: '#6FC99C' }]}>
              {data[0].count}
            </Text>
            <Text style={[cardStyles.boxText, { color: '#6FC99C' }]}>
              {data[0].title}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={cardStyles.boxContainer}
            onPress={() => this.props.onPressed(data[1].title)}
          >
            <Text style={[cardStyles.boxNumber, { color: '#F5A623' }]}>
              {data[1].count}
            </Text>
            <Text style={[cardStyles.boxText, { color: '#F5A623' }]}>
              {data[1].title}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={cardStyles.boxContainer}
            onPress={() => this.props.onPressed(data[2].title)}
          >
            <Text style={[cardStyles.boxNumber, { color: '#4B91E2' }]}>
              {data[2].count}
            </Text>
            <Text style={[cardStyles.boxText, { color: '#4B91E2' }]}>
              {data[2].title}
            </Text>
          </TouchableOpacity>
        </View>
        {/* bootom part */}
      </View>
    );
  }
}

const cardStyles = StyleSheet.create({
  body: {
    height: 288,
    width: '100%',
    backgroundColor: colors.white,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'column',
  },
  profileImage: {
    width: 96,
    height: 96,
    borderRadius: 48,
    borderColor: colors.main,
    borderWidth: 5,
    marginTop: 35,
  },
  overlay: {
    position: 'absolute',
    bottom: 0,
    right: 0,
  },
  text: {
    fontFamily: fonts.PrivaProTwo,
    fontSize: 18,
    color: colors.black,
    marginTop: 12,
    maxWidth: '100%',
  },
  // bottom part
  boxContainer: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: '33.33%',
    height: 100,
  },
  boxNumber: {
    fontFamily: fonts.PrivaProFour,
    fontSize: 32,
  },
  boxText: {
    fontFamily: fonts.PrivaProTwo,
    fontSize: 14,
  },
});
