import React, { PureComponent } from 'react';
import { Text, View, StyleSheet, Switch } from 'react-native';
import { colors, fonts } from '../../theme';

export class QRCode extends PureComponent {
  render() {
    const { sValue, toggleSwitch } = this.props;
    return (
      <View style={styles.container}>
        <View>
          <Text style={styles.title}>QR Code</Text>
          <Text style={styles.subTitle}>Lorem Ipsum dolor sit amet</Text>
        </View>
        <View>
          <Switch value={sValue} onValueChange={toggleSwitch} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 75,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: colors.white,
    marginTop: 8,
    marginBottom: 8,
    padding: 18,
    shadowColor: 'rgba(0, 0, 0, 0.25)',
    shadowOffset: { width: 0, height: 1 },
    shadowRadius: 4,
    elevation: 1,
  },
  title: {
    fontFamily: fonts.PrivaProThree,
    fontSize: 16,
    color: 'rgba(0, 0, 0, 0.87)',
  },
  subTitle: {
    fontFamily: fonts.PrivaProOne,
    fontSize: 14,
    color: 'rgba(0, 0, 0, 0.5438)',
  },
});
