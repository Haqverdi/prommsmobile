/**
 * Profile page modal and other components
 */
import { InfoCard } from './InfoCard';
import ProfileCard from './ProfileCard';
import { QRCode } from './QRCode';
import { Popup } from './Popup';

export { InfoCard, ProfileCard, QRCode, Popup };
