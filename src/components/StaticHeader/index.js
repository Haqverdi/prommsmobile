/**
 * Static header component for other screens(excepr Home page)
 */

import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { fonts, colors } from '../../theme';

const StaticHeader = ({ title }) => (
  <View style={styles.container}>
    <Text style={styles.text}>{title}</Text>
  </View>
);

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    flex: 1,
    backgroundColor: colors.main,
  },
  text: {
    color: '#FFFFFF',
    fontFamily: fonts.PrivaProThree,
    fontSize: 18,
  },
});

export default StaticHeader;
