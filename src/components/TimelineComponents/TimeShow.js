/**
 * home page card modal timeline part 'time showing' part
 */
import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import { colors, fonts, GreydotIcon } from '../../theme';
import PropTypes from 'prop-types';

// timeline time
export const TimeShow = ({ position, time }) => {
  return (
    <View style={styles.timeContainer}>
      {position == 'start' && <IconsFirst />}
      {position == 'center' && <Icons />}
      {position == 'end' && <IconsLast />}
      <View style={styles.timeTextContainer}>
        <Text style={styles.timeText}>{time}</Text>
      </View>
    </View>
  );
};

TimeShow.propTypes = {
  position: PropTypes.string,
  time: PropTypes.string,
};

// timeline middle
const Icons = () => {
  return (
    <View style={styles.icon}>
      <GreydotIcon />
      <Image source={require('../../assets/img/timelineTime.png')} />
      <GreydotIcon />
    </View>
  );
};

// timeline end
const IconsLast = () => {
  return (
    <View style={styles.lastIcon}>
      <GreydotIcon />
      <Image source={require('../../assets/img/timelineTime.png')} />
    </View>
  );
};

// timeline start
const IconsFirst = () => {
  return (
    <View style={styles.firstIcon}>
      <Image source={require('../../assets/img/timelineTime.png')} />
      <GreydotIcon />
    </View>
  );
};

const styles = StyleSheet.create({
  icon: {
    backgroundColor: colors.background,
    height: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingRight: 9,
  },
  lastIcon: {
    backgroundColor: colors.background,
    height: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingRight: 9,
    paddingBottom: 24,
  },
  firstIcon: {
    backgroundColor: colors.background,
    height: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingRight: 9,
    paddingTop: 12,
  },
  timeContainer: {
    height: 34,
    width: 94,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: colors.background,
    marginBottom: 8,
    marginLeft: 6,
  },
  timeTextContainer: {
    height: 34,
    width: 68,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#6FC99C',
    borderRadius: 30,
    marginLeft: 10,
  },
  timeText: {
    fontFamily: fonts.PrivaProThree,
    fontSize: 16,
    color: colors.white,
  },
});
