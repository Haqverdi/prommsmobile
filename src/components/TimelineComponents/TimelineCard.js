/**
 * Home page card modal timeline part, timeline card component
 */

import React, { PureComponent } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { colors, fonts, IconGenerator, GreydotIcon } from '../../theme';
import PropTypes from 'prop-types';

export class TimelineCard extends PureComponent {
  static propTypes = {
    last: PropTypes.bool,
    data: PropTypes.object,
  };

  render() {
    const { last } = this.props;
    const { title, name, time, status } = this.props.data;
    return (
      <View style={styles.container}>
        {last ? <IconsLast icon={status} /> : <Icons icon={status} />}
        <View style={styles.card}>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.name}>{name}</Text>
          <Text style={styles.date}>{time}</Text>
        </View>
      </View>
    );
  }
}

// timeline middle
const Icons = ({ icon }) => {
  return (
    <View style={styles.icon}>
      <GreydotIcon />
      <GreydotIcon />
      <GreydotIcon />
      <IconGenerator name={icon} width={24} height={24} />
      <GreydotIcon />
      <GreydotIcon />
      <GreydotIcon />
    </View>
  );
};

// timeline end
const IconsLast = ({ icon }) => {
  return (
    <View style={styles.lastIcon}>
      <GreydotIcon />
      <GreydotIcon />
      <GreydotIcon />
      <IconGenerator name={icon} width={24} height={24} />
    </View>
  );
};

// timeline start
const IconsFirst = ({ icon }) => {
  return (
    <View style={styles.firstIcon}>
      <IconGenerator name={icon} width={24} height={24} />
      <GreydotIcon />
      <GreydotIcon />
      <GreydotIcon />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 68,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: colors.white,
    marginBottom: 8,
    // shadowColor: '#000',
    // shadowOffset: { width: 2, height: 2 },
    // shadowOpacity: 0.5,
    // shadowRadius: 2,
    // elevation: 1,
  },
  card: {
    flexDirection: 'column',
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  title: {
    fontFamily: fonts.PrivaProFour,
    fontSize: 16,
    color: colors.black,
    lineHeight: 20,
  },
  name: {
    fontFamily: fonts.PrivaProTwo,
    fontSize: 12,
    color: '#BBBBBB',
    lineHeight: 16,
  },
  date: {
    fontFamily: fonts.PrivaProTwo,
    fontSize: 10,
    color: '#51AB7E',
    lineHeight: 16,
  },
  icon: {
    backgroundColor: colors.background,
    height: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingRight: 9,
  },
  lastIcon: {
    backgroundColor: colors.background,
    height: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingRight: 9,
    paddingBottom: 24,
  },
  firstIcon: {
    backgroundColor: colors.background,
    height: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingRight: 9,
    paddingTop: 12,
  },
});
