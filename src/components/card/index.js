/*
  Home page card component (...with swipe option) and job status
*/
import React, { PureComponent } from 'react';
import {
  StyleSheet,
  Image,
  TouchableOpacity,
  TouchableHighlight,
  Platform,
  PanResponder,
} from 'react-native';
import { Text, View, SwipeRow } from 'native-base';
import { colors, fonts, AcceptIcon, CancelIcon } from '../../theme';
import PropTypes from 'prop-types';

export default class MyCard extends PureComponent {
  constructor(props) {
    super(props);
    this.panresponder = PanResponder.create({
      onPanResponderTerminate: () => false,
    });
    this.state = {
      icons: {
        done: require('../../assets/img/cardImages/done.png'),
        paused: require('../../assets/img/cardImages/paused.png'),
        reject: require('../../assets/img/cardImages/reject.png'),
        timer: require('../../assets/img/cardImages/timer.png'),
      },
    };
  }

  static propTypes = {
    icon: PropTypes.string,
  };
  render() {
    const icon = this.state.icons[this.props.icon];
    const { _swipeButtonsPressHandle } = this.props;
    return (
      <SwipeRow
        closeOnRowPress={true}
        style={styles.container}
        rightOpenValue={-112}
        list={true}
        disableRightSwipe
        // card main
        body={
          <TouchableOpacity
            onPress={() => this.props.onCardClick()}
            style={styles.body}
          >
            {/* step icon */}
            <View style={styles.icon}>
              <Image source={icon} />
            </View>
            {/* body */}
            <View style={styles.main}>
              <Text style={styles.title}>AcWorks</Text>
              <Text style={styles.subtitle}>Assests</Text>
            </View>
            {/* time info */}
            <View style={styles.third}>
              <Text style={styles.number}>1235</Text>
              <Text style={styles.time}>17:00PM</Text>
            </View>
          </TouchableOpacity>
        }
        // swipe part
        right={
          <View style={styles.swipeElementsContainer}>
            <TouchableHighlight
              style={{ width: '50%' }}
              onPress={() => alert('Accept')}
            >
              <View style={styles.swipeElements}>
                <Text style={styles.swipeElementsText}>Accept</Text>
                <AcceptIcon />
              </View>
            </TouchableHighlight>
            <TouchableHighlight
              style={{ width: '50%' }}
              onPress={() => _swipeButtonsPressHandle('cancel', 1)}
            >
              <View style={styles.swipeElements}>
                <Text style={styles.swipeElementsText}>Cancel</Text>
                <CancelIcon />
              </View>
            </TouchableHighlight>
          </View>
        }
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    minHeight: 72,
    justifyContent: 'center',
    marginBottom: 4,
    paddingHorizontal: 0,
    marginHorizontal: 0,
    shadowColor: '#000',
    elevation: 0,
  },
  body: {
    minHeight: 72,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    padding: 16,
  },
  icon: {
    flex: 1,
  },
  main: {
    flex: 4,
  },
  title: {
    fontSize: 18,
    color: '#464A4B',
    fontWeight: 'bold',
    marginBottom: 6,
    lineHeight: 24,
    fontFamily: fonts.RobotoBold,
  },
  subtitle: {
    fontSize: 14,
    lineHeight: 22,
    color: '#464A4B',
    fontFamily: fonts.RobotoRegular,
  },
  third: {
    flex: 1,
    alignItems: 'flex-end',
  },
  number: {
    color: colors.main,
    fontSize: 12,
    marginBottom: 6,
    fontFamily: fonts.PrivaProThree,
  },
  time: {
    color: '#A0A4A5',
    fontSize: 12,
    fontFamily: fonts.PrivaProTwo,
  },
  // swipe options styles
  swipeElementsContainer: {
    height: 84,
    width: 112,
    flexDirection: 'row',
  },

  swipeElements: {
    height: Platform.OS === 'ios' ? 84 : '100%',
    elevation: 0,
    flex: 1,
    margin: 0,
    padding: 0,
    paddingTop: 18,
    paddingBottom: 9,
    flexDirection: 'column-reverse',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: colors.white,
    borderLeftColor: colors.greyText,
    borderLeftWidth: 0.6,
  },
  swipeElementsText: {
    fontSize: 12,
    fontFamily: fonts.RobotoRegular,
    color: colors.greyText,
  },
});
