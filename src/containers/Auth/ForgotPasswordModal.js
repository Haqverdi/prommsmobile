import React from 'react';
import { View, Text, Modal } from 'react-native';
import { Container } from 'native-base';
import { SimpleHeader } from '../../components/Header/SimpleHeader';

export const ForgotPasswordModal = ({
  modalCloseHandle,
  modalVisibleStatus,
}) => {
  return (
    <Modal
      animationType="slide"
      onRequestClose={modalCloseHandle}
      presentationStyle="fullScreen"
      transparent={false}
      visible={modalVisibleStatus}
    >
      <Container>
        <SimpleHeader
          title="Forgot password"
          navigationHandle={() => modalCloseHandle()}
        />
      </Container>
    </Modal>
  );
};
