import React from 'react';
import Dialog, {
  DialogContent,
  SlideAnimation,
} from 'react-native-popup-dialog';

export default function Popup({ popupVisible, handleClose, children }) {
  return (
    <Dialog
      visible={popupVisible}
      onTouchOutside={() => handleClose()}
      dialogAnimation={
        new SlideAnimation({
          toValue: 0,
          slideFrom: 'bottom',
          useNativeDriver: true,
        })
      }
      dialogStyle={{
        borderRadius: 4,
      }}
    >
      <DialogContent style={{ paddingTop: 0, paddingBottom: 0 }}>
        {children}
      </DialogContent>
    </Dialog>
  );
}
