import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  TextInput,
  TouchableOpacity,
  Keyboard,
  Alert,
  StatusBar,
} from 'react-native';
import { Container, Button } from 'native-base';
import PropTypes from 'prop-types';
import { colors, fonts } from '../../theme/index';
import Dialog from 'react-native-dialog';
import { observer, inject } from 'mobx-react';
import * as Animatable from 'react-native-animatable';
import {
  emailVerification,
  passwordVerification,
} from '../../utils/inputValidations';
import Popup from './Popup';
import Loading from '../../utils/Loading';

class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisibleStatus: false,
      forgotPasswordSendModalVisible: false,
      email: '',
      password: '',
      emailError: false,
      passwordError: false,
      emailForForgotPassword: '',
      emailForForgotPasswordError: false,
    };
  }

  // onchange text updated state
  inputsHandle = (name, value) => {
    this.setState({
      [name]: value,
    });
  };
  // modal close and delete value in state
  modalCloseHandle = () => {
    this.setState(({ modalVisibleStatus }) => ({
      modalVisibleStatus: !modalVisibleStatus,
      emailForForgotPassword: '',
    }));
  };

  // submit
  handleSubmit = () => {
    // validate inputs
    const passwordIsValidLength = passwordVerification(this.state.password);
    const emailIsValid = emailVerification(this.state.email);
    // if error animate err message
    this.transitionErrorMessage('email', !emailIsValid);
    this.transitionErrorMessage('passwordContainer', !passwordIsValidLength);
    // after show error message with little time off
    setTimeout(() => {
      this.setState({
        emailError: !emailIsValid,
        passwordError: !passwordIsValidLength,
      });
    }, 150);
  };
  // transition error message showing
  transitionErrorMessage = (refName, checker) => {
    this.refs[refName].transitionTo({ height: checker ? 93 : 72 }, 300);
  };

  // forgotpassword send handle
  forgotPasswordSend = () => {
    // check valid email
    const isValid = emailVerification(this.state.emailForForgotPassword);
    if (isValid && this.state.emailForForgotPassword.length > 3) {
      this.setState({
        forgotPasswordSendModalVisible: true,
        modalVisibleStatus: false,
        emailForForgotPassword: '',
      });
    } else {
      Alert.alert('Email formatı düzgün deyil');
    }
  };

  render() {
    // console.warn(this.props.AuthStore);
    // data from store
    const { loading, error } = this.props.AuthStore;
    return (
      <TouchableWithoutFeedback
        style={signInStyles.container}
        onPress={Keyboard.dismiss}
      >
        <Container style={signInStyles.container}>
          <StatusBar barStyle="default" />
          {/* header */}
          <View style={signInStyles.headerContainer}>
            <Text style={signInStyles.header}>Daxil ol</Text>
            <Text style={signInStyles.subHeader}>Qeydiyyatdan keçdiyiniz</Text>
            <Text style={signInStyles.subHeader}>
              email ünvanını və şifrəni daxil edin.
            </Text>
          </View>
          {/* Email field */}
          <Animatable.View
            style={[
              signInStyles.inputContainer,
              loading && signInStyles.loading,
            ]}
            ref={'email'}
          >
            <Text style={signInStyles.label}>Email</Text>
            <TextInput
              value={this.state.email}
              autoCorrect={false}
              enablesReturnKeyAutomatically
              textContentType="emailAddress"
              autoCapitalize="none"
              keyboardType="email-address"
              style={signInStyles.input}
              returnKeyType="next"
              onSubmitEditing={() => this.refs.txtPassword.focus()}
              onChangeText={text => {
                this.inputsHandle('email', text);
              }}
            />
            {this.state.emailError && (
              <Text style={{ marginTop: 4 }}>Email formatı düzgün deyil.</Text>
            )}
          </Animatable.View>
          {/* Error view hadnling */}
          {error &&
            Alert.alert('Xəta baş verdi!', 'Zəhmət olmasa, yenidən cəhd edin', [
              { text: 'OK' },
            ])}
          {/* Password */}
          <Animatable.View
            style={[
              signInStyles.inputContainer,
              loading && signInStyles.loading,
            ]}
            ref={'passwordContainer'}
          >
            <Text style={signInStyles.label}>Şifrə</Text>
            <TextInput
              value={this.state.password}
              secureTextEntry={true}
              enablesReturnKeyAutomatically
              textContentType="password"
              style={signInStyles.input}
              returnKeyType="go"
              autoCapitalize="none"
              autoCorrect={false}
              ref={'txtPassword'}
              onChangeText={text => {
                this.inputsHandle('password', text);
              }}
            />
            {this.state.passwordError && (
              <Text style={{ marginTop: 4 }}>
                Şifrə ən az 6 simvol olmalıdır.
              </Text>
            )}
          </Animatable.View>
          {/* Forgot paswword, opens alert */}
          <View
            style={[
              signInStyles.forgotPasswordContainer,
              loading && signInStyles.loading,
            ]}
          >
            <TouchableOpacity onPress={() => this.modalCloseHandle()}>
              <Text style={signInStyles.forgotPasswordText}>
                Şifrəni unutmuşam
              </Text>
            </TouchableOpacity>
          </View>
          {/* Sign in button */}
          <View
            style={[
              signInStyles.inputContainer,
              loading && signInStyles.loading,
            ]}
          >
            <Button
              full
              style={signInStyles.signInButton}
              onPress={this.handleSubmit}
              disabled={loading}
            >
              <Text style={signInStyles.signInButtonText}>Daxil ol</Text>
            </Button>
          </View>
          {/* SIgn up */}
          <View style={loading && signInStyles.loading}>
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                height: 48,
              }}
              onPress={() => this.props.navigation.navigate('SignUp')}
            >
              <Text style={signInStyles.singUpPassiveText}>
                Hesabınız yoxdur ?{' '}
              </Text>
              <Text style={signInStyles.singUpActiveText}>
                {' '}
                Qeydiyyatdan keç!
              </Text>
            </TouchableOpacity>
          </View>
          {/* Modal */}
          {/* <ForgotPasswordModal
            modalVisibleStatus={this.state.modalVisibleStatus}
            modalCloseHandle={this.modalCloseHandle}
          /> */}
          <View>
            <Dialog.Container
              visible={this.state.modalVisibleStatus}
              key="firstalert"
            >
              <Dialog.Title>Şifrənin bərpası</Dialog.Title>
              <Dialog.Description>
                şifrəni bərpa etmək üçün email ünvanınızı daxil edin
              </Dialog.Description>
              <Dialog.Input
                value={this.state.emailForForgotPassword}
                textContentType="emailAddress"
                keyboardType="email-address"
                placeholder="Email"
                autoCapitalize="none"
                enablesReturnKeyAutomatically
                returnKeyType="go"
                autoCorrect={false}
                onSubmitEditing={this.forgotPasswordSend}
                onChangeText={text => {
                  this.inputsHandle('emailForForgotPassword', text);
                }}
              />
              <Dialog.Button label="İmtina" onPress={this.modalCloseHandle} />
              <Dialog.Button label="Göndər" onPress={this.forgotPasswordSend} />
            </Dialog.Container>
          </View>
          {/* forgotPasswordSendModalVisible */}
          <Popup
            popupVisible={this.state.forgotPasswordSendModalVisible}
            handleClose={() => {
              this.setState({
                forgotPasswordSendModalVisible: false,
              });
            }}
          >
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                height: 56,
              }}
            >
              <Text style={signInStyles.subHeader}>
                Şifrənin bərpası üçün sorğu emailə göndərildi
              </Text>
            </View>
          </Popup>
          {/* loading indicator */}
          {loading && <Loading />}
        </Container>
      </TouchableWithoutFeedback>
    );
  }
}

export default inject('AuthStore')(observer(SignIn));

const signInStyles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.background,
  },
  inputContainer: {
    width: '100%',
    paddingHorizontal: 32,
    marginBottom: 16,
  },
  loading: {
    opacity: 0.3,
  },
  label: {
    fontFamily: fonts.RobotoRegular,
    fontSize: 14,
    color: 'rgba(0, 0, 0, 0.38)',
    lineHeight: 16,
    marginBottom: 8,
  },
  input: {
    height: 48,
    backgroundColor: colors.white,
    width: '100%',
    paddingHorizontal: 12,
    paddingLeft: 24,
    borderRadius: 2,
    fontFamily: fonts.RobotoRegular,
    color: colors.black,
    fontSize: 16,
  },
  // header
  headerContainer: {
    justifyContent: 'center',
    width: '100%',
    alignItems: 'center',
    marginBottom: 56,
  },
  header: {
    fontFamily: fonts.RobotoBold,
    fontSize: 24,
    lineHeight: 32,
    color: colors.black,
    marginBottom: 24,
  },
  subHeader: {
    fontFamily: fonts.RobotoRegular,
    fontSize: 16,
    lineHeight: 24,
    color: colors.black,
  },
  // forgot password
  forgotPasswordContainer: {
    width: '100%',
    height: 48,
    paddingHorizontal: 32,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  forgotPasswordText: {
    fontFamily: fonts.RobotoRegular,
    fontSize: 16,
    lineHeight: 24,
    color: '#4B91E2',
  },
  // sign in
  signInButton: {
    height: 48,
    marginTop: 16,
    marginBottom: 8,
    backgroundColor: colors.main,
  },
  signInButtonText: {
    fontFamily: fonts.RobotoMedium,
    fontSize: 16,
    lineHeight: 24,
    color: colors.white,
  },
  // sign up styles
  singUpPassiveText: {
    fontFamily: fonts.RobotoRegular,
    fontSize: 16,
    lineHeight: 24,
    color: colors.black,
  },
  singUpActiveText: {
    fontFamily: fonts.RobotoBold,
    fontSize: 16,
    lineHeight: 24,
    color: '#4B91E2',
  },
});
