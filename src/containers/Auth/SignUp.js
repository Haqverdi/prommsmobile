import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  TextInput,
  TouchableOpacity,
  Keyboard,
  Alert,
  StatusBar,
  ActivityIndicator,
} from 'react-native';
import { Container, Button, Item, Input, Icon } from 'native-base';
import PropTypes from 'prop-types';
import { colors, fonts } from '../../theme/index';
import * as Animatable from 'react-native-animatable';
import { observer, inject } from 'mobx-react';
import Loading from '../../utils/Loading';
import {
  emailVerification,
  passwordVerification,
} from '../../utils/inputValidations';
import Dialog, { DialogContent } from 'react-native-popup-dialog';

class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      passwordRepeat: '',
      emailError: false,
      passwordError: false,
      passwordRepeatError: false,
      repeatPasswordIsValid: false,
      logged: false,
      timeout: null,
    };
  }

  // onchange text updated state
  inputsHandle = (name, value) => {
    this.setState({
      [name]: value,
    });
  };

  // reapeat password change handle, for icon showing and other visual work
  repeatPasswordOnChange = value => {
    this.setState(
      {
        passwordRepeat: value,
      },
      () => {
        const repeatPasswordIsValid =
          this.state.password === this.state.passwordRepeat;
        this.setState({
          passwordRepeatError: !repeatPasswordIsValid,
          repeatPasswordIsValid: repeatPasswordIsValid,
        });
      }
    );
  };

  // submit
  handleSubmit = () => {
    // validate inputs
    const passwordIsValidLength = passwordVerification(this.state.password);
    const emailIsValid = emailVerification(this.state.email);
    const repeatPasswordIsValid =
      this.state.password === this.state.passwordRepeat;
    // update state
    // if error animate err message
    this.transitionErrorMessage('email', !emailIsValid);
    this.transitionErrorMessage('passwordContainer', !passwordIsValidLength);
    // after show error message with little time off
    this.timeout = setTimeout(() => {
      this.setState({
        emailError: !emailIsValid,
        passwordError: !passwordIsValidLength,
        passwordRepeatError: !repeatPasswordIsValid,
      });
    }, 150);
    // all ok, send request
    if (passwordIsValidLength && emailIsValid && repeatPasswordIsValid) {
      this.props.AuthStore.register(
        this.state.email,
        this.state.password,
        this.state.passwordRepeat
      );
    }
  };
  // transition error message showing
  transitionErrorMessage = (refName, checker) => {
    this.refs[refName].transitionTo({ height: checker ? 93 : 72 }, 300);
  };

  componentDidUpdate = prevProps => {
    if (this.props.AuthStore.logged && !this.props.AuthStore.loading) {
      setTimeout(() => {
        this.props.navigation.navigate('App');
      });
    }
  };

  // clear timeout on unmount
  componentWillUnmount = () => {
    clearTimeout(this.timeout);
  };

  render() {
    const { error, loading } = this.props.AuthStore;
    const {
      repeatPasswordIsValid,
      email,
      password,
      passwordRepeat,
      emailError,
      passwordError,
      passwordRepeatError,
    } = this.state;
    return (
      <TouchableWithoutFeedback
        style={signupStyles.container}
        onPress={Keyboard.dismiss}
      >
        <Container style={signupStyles.container}>
          <StatusBar barStyle="default" />
          {/* header */}
          <View style={signupStyles.headerContainer}>
            <Text style={signupStyles.header}>Qeydiyyat</Text>
            <Text style={signupStyles.subHeader}>
              İşlək emaildən istifadə edərək
            </Text>
            <Text style={signupStyles.subHeader}>qeydiyyatdan keç!</Text>
          </View>
          {/* Email field */}
          <Animatable.View style={signupStyles.inputContainer} ref={'email'}>
            <Text style={signupStyles.label}>Email</Text>
            <TextInput
              value={email}
              autoCorrect={false}
              autoCapitalize="none"
              enablesReturnKeyAutomatically
              textContentType="emailAddress"
              keyboardType="email-address"
              style={signupStyles.input}
              returnKeyType="next"
              onSubmitEditing={() => this.refs.txtPassword.focus()}
              onChangeText={text => {
                this.inputsHandle('email', text);
              }}
            />
            {/* email error check */}
            {emailError && (
              <Text style={{ marginTop: 4 }}>Email formatı düzgün deyil.</Text>
            )}
          </Animatable.View>
          {/* loading indicator */}
          <Loading visible={loading} />
          {/* API Error view hadnling */}
          {error &&
            Alert.alert('Xəta baş verdi!', 'Zəhmət olmasa, yenidən cəhd edin', [
              { text: 'OK' },
            ])}
          {/* Password field */}
          <Animatable.View
            style={signupStyles.inputContainer}
            ref={'passwordContainer'}
          >
            <Text style={signupStyles.label}>Şifrə</Text>
            <TextInput
              value={password}
              secureTextEntry={true}
              enablesReturnKeyAutomatically
              textContentType="password"
              style={signupStyles.input}
              returnKeyType="next"
              autoCorrect={false}
              ref={'txtPassword'}
              onSubmitEditing={() => this.refs.passwordRepeat._root.focus()}
              onChangeText={text => {
                this.inputsHandle('password', text);
              }}
            />
            {passwordError && (
              <Text style={{ marginTop: 4 }}>
                Şifrə ən az 6 simvol olmalıdır.
              </Text>
            )}
          </Animatable.View>
          {/* password repeat, nativebase components used, with custom style */}
          <Animatable.View
            style={signupStyles.inputContainer}
            ref={'passwordRepeatContainer'}
          >
            <Text style={signupStyles.label}>Şifrənin təkrarı</Text>
            <Item
              success={repeatPasswordIsValid}
              style={[signupStyles.input, { borderBottomWidth: 0 }]}
            >
              <Input
                value={passwordRepeat}
                secureTextEntry={true}
                textContentType="password"
                style={{ paddingBottom: 0, elevation: 0, paddingLeft: 0 }}
                returnKeyType="go"
                enablesReturnKeyAutomatically
                autoCorrect={false}
                ref={'passwordRepeat'}
                onSubmitEditing={() => this.handleSubmit}
                onChangeText={text => {
                  this.repeatPasswordOnChange(text);
                }}
              />
              {/* icon show if repeat pass correct */}
              {repeatPasswordIsValid && (
                <Icon name="checkmark-circle" color={colors.main} />
              )}
            </Item>
            {/* repeat pass error */}
            {passwordRepeatError && (
              <Text style={{ marginTop: 4 }}>
                Təkrar şifrə düzgün daxil edilməyib.
              </Text>
            )}
          </Animatable.View>
          {/* signup button */}
          <View style={signupStyles.inputContainer}>
            <Button
              full
              style={signupStyles.signInButton}
              onPress={() => this.handleSubmit()}
              disabled={loading}
            >
              <Text style={signupStyles.signInButtonText}>Qeydiyyat</Text>
            </Button>
          </View>
          {/* or SIgn in */}
          <View style={signupStyles.orSignInContainer}>
            <TouchableOpacity
              style={signupStyles.orSignInTouchable}
              onPress={() => this.props.navigation.navigate('SignIn')}
            >
              <Text style={signupStyles.singUpPassiveText}>və ya</Text>
              <Text style={signupStyles.singUpActiveText}> Daxil ol!</Text>
            </TouchableOpacity>
          </View>
        </Container>
      </TouchableWithoutFeedback>
    );
  }
}

export default inject('AuthStore')(observer(SignUp));

// styles
const signupStyles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.background,
  },
  inputContainer: {
    width: '100%',
    paddingHorizontal: 32,
    marginBottom: 16,
  },
  loading: {
    opacity: 0.3,
  },
  label: {
    fontFamily: fonts.RobotoRegular,
    fontSize: 14,
    color: 'rgba(0, 0, 0, 0.38)',
    lineHeight: 16,
    marginBottom: 8,
  },
  input: {
    height: 48,
    backgroundColor: colors.white,
    width: '100%',
    paddingHorizontal: 12,
    paddingLeft: 24,
    borderRadius: 2,
    fontFamily: fonts.RobotoRegular,
    color: colors.black,
    fontSize: 16,
  },
  // header
  headerContainer: {
    justifyContent: 'center',
    width: '100%',
    alignItems: 'center',
    marginBottom: 32,
  },
  header: {
    fontFamily: fonts.RobotoBold,
    fontSize: 24,
    lineHeight: 32,
    color: colors.black,
    marginBottom: 16,
  },
  subHeader: {
    fontFamily: fonts.RobotoRegular,
    fontSize: 16,
    lineHeight: 24,
    color: colors.black,
  },
  // forgot password
  forgotPasswordContainer: {
    width: '100%',
    height: 48,
    paddingHorizontal: 32,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  forgotPasswordText: {
    fontFamily: fonts.RobotoRegular,
    fontSize: 16,
    lineHeight: 24,
    color: '#4B91E2',
  },
  // sign up
  signInButton: {
    height: 48,
    marginTop: 16,
    marginBottom: 8,
    backgroundColor: colors.main,
  },
  signInButtonText: {
    fontFamily: fonts.RobotoMedium,
    fontSize: 16,
    lineHeight: 24,
    color: colors.white,
  },
  // or sign in styles
  orSignInContainer: {
    width: '100%',
    alignItems: 'center',
  },
  orSignInTouchable: {
    flexDirection: 'row',
    width: '70%',
    justifyContent: 'center',
  },
  singUpPassiveText: {
    fontFamily: fonts.RobotoRegular,
    fontSize: 16,
    lineHeight: 24,
    color: colors.black,
  },
  singUpActiveText: {
    fontFamily: fonts.RobotoBold,
    fontSize: 16,
    lineHeight: 24,
    color: '#4B91E2',
  },
});
