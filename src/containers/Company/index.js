/**
 * Company container
 */

import React, { PureComponent } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import {
  Container,
  Content,
  Card,
  CardItem,
  Body,
  Thumbnail,
  Spinner,
} from 'native-base';
import { connect } from 'react-redux';
import {
  getCompanies,
  changePrimary,
} from '../../store/actions/companiesActions';
import { colors, fonts } from '../../theme';
import { PropTypes } from 'prop-types';
import MyHeader from '../../components/Header/Header';

class Company extends PureComponent {
  constructor(props) {
    super(props);
    if (this.props.companies.companies.length == 0) {
      this.props.getCompanies();
    }
  }

  static propTypes = {
    companies: PropTypes.object,
    getCompanies: PropTypes.func,
    changePrimary: PropTypes.func,
  };
  // top bar options (react navigation)
  static navigationOptions = {
    header: null,
  };

  // primary company change
  handleCompanyChange = id => {
    this.props.changePrimary(id);
  };

  // check companies from store
  // componentDidMount = () => {
  //   if (this.props.companies.companies.length == 0) {
  //     this.props.getCompanies();
  //   }
  // };

  render() {
    const { companies } = this.props;
    return (
      <Container style={styles.container}>
        {/* Header */}
        <MyHeader navigation={this.props.navigation} title="Company" />
        {/* Content */}
        <Content padder contentContainerStyle={{ padding: 16 }}>
          {companies.loading && <Spinner color={colors.main} />}
          {/* render companies list */}
          {companies.companies.map(item => {
            return (
              <CompanyCards
                key={item.id}
                {...item}
                companyChange={() => this.handleCompanyChange(item.id)}
              />
            );
          })}
        </Content>
      </Container>
    );
  }
}

// card for each company
const CompanyCards = ({ title, logo, companyChange, selected }) => {
  // check icon for selected company
  const selectedImage = selected
    ? require('../../assets/img/companies/selected.png')
    : require('../../assets/img/companies/notSelected.png');
  return (
    <TouchableOpacity
      key={title}
      style={{ marginBottom: 8 }}
      onPress={() => companyChange()}
    >
      <Card style={styles.card}>
        <CardItem>
          <Body style={styles.cardBody}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Thumbnail
                source={{ uri: logo }}
                style={{
                  width: 40,
                  height: 40,
                  padding: 0,
                  margin: 0,
                }}
              />
              <Text style={styles.cardText}>{title}</Text>
            </View>
            <Image source={selectedImage} />
          </Body>
        </CardItem>
      </Card>
    </TouchableOpacity>
  );
};

CompanyCards.propTypes = {
  title: PropTypes.string,
  logo: PropTypes.string,
  companyChange: PropTypes.func,
  selected: PropTypes.bool,
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.background,
  },
  card: {
    height: 72,
    marginBottom: 0,
  },
  cardBody: {
    height: 46,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  cardText: {
    color: colors.black,
    fontFamily: fonts.PrivaProThree,
    fontSize: 16,
    lineHeight: 20,
    textAlign: 'left',
    letterSpacing: 0.5,
    marginLeft: 16,
  },
});

// redux connection ;)
const mapStateToProps = state => ({
  companies: state.companiesReducer,
});

export default connect(
  mapStateToProps,
  {
    getCompanies,
    changePrimary,
  }
)(Company);
