import React, { PureComponent } from 'react';
import { StyleSheet, Modal } from 'react-native';
import { Container } from 'native-base';
import PropTypes from 'prop-types';
import { SimpleHeader } from '../../components/Header/SimpleHeader';

export class CancelReasonModal extends PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    const { cancelReasonModalVisible, cancelModalHandle, cardId } = this.props;
    return (
      <Modal
        animationType="slide"
        onRequestClose={cancelModalHandle}
        presentationStyle="fullScreen"
        transparent={false}
        visible={cancelReasonModalVisible}
      >
        <Container>
          <SimpleHeader
            title="Reason"
            navigationHandle={() => cancelModalHandle()}
          />
        </Container>
      </Modal>
    );
  }
}

// proptypes
CancelReasonModal.propTypes = {
  cancelReasonModalVisible: PropTypes.bool,
  cancelModalHandle: PropTypes.func,
};

// styles
const CancelReasonStyles = StyleSheet.create({});
