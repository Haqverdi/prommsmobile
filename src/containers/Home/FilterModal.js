/**
 * Filter Modal opening from header
 */
import React, { Fragment, PureComponent } from 'react';
import Carousel from 'react-native-snap-carousel';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  ScrollView,
} from 'react-native';
import Modal from 'react-native-modal';
import { colors, fonts, SwipeIcon } from '../../theme';

const Dwidth = Dimensions.get('window').width;
const Dheight = Dimensions.get('window').height;

export default class FilterModal extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      selectedFilter: [],
    };
  }

  // filter main content
  _renderItem = ({ item, index }) => {
    return (
      <View
        style={{
          width: '100%',
          height: '100%',
          backgroundColor: colors.white,
        }}
        key={index}
      >
        <View style={{ paddingHorizontal: 16, paddingTop: 16 }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}
          >
            {/* clear button */}
            <TouchableOpacity
              style={{
                width: '30%',
              }}
              onPress={() => this._clearSelectedItems()}
            >
              <Text style={carouselStyles.clearText}>Clear</Text>
              {/* apply button */}
            </TouchableOpacity>
            <Text style={carouselStyles.titleText}>{item.title}</Text>
            <TouchableOpacity
              style={{
                width: '30%',
                alignItems: 'flex-end',
              }}
            >
              <Text style={carouselStyles.applyText}>Apply</Text>
            </TouchableOpacity>
          </View>
          <ScrollView
            contentContainerStyle={{
              flexWrap: 'wrap',
              flexDirection: 'row',
              height: Dheight / 2 - 120,
              width: '100%',
              maxWidth: '100%',
              maxHeight: '100%',
              marginTop: 25,
              justifyContent: 'space-around',
              alignContent: 'space-around',
            }}
          >
            {item.filters.map((item, index) => (
              <CustomCheckBoxes
                key={index}
                active={
                  this.state.selectedFilter.includes(item.name) ? true : false
                }
                name={item.name}
                _filterItemPressHandler={this._filterItemPressHandler}
              />
            ))}
          </ScrollView>
          <View />
        </View>
      </View>
    );
  };

  // handle filter button press
  _filterItemPressHandler = name => {
    this.setState({
      selectedFilter: this.state.selectedFilter.includes(name)
        ? this.state.selectedFilter.filter(item => item != name)
        : [...this.state.selectedFilter, name],
    });
  };

  // clear all selected filter
  _clearSelectedItems = () => {
    this.setState({
      selectedFilter: [],
    });
  };

  // apply selected filters
  _applySelectedItems = () => {};

  render() {
    const fakeData = [
      {
        title: 'Test',
        filters: [
          {
            name: 'CheckBox1',
          },
          {
            name: 'CheckBox2',
          },
          {
            name: 'CheckBox3',
          },
          {
            name: 'CheckBox4',
          },
          {
            name: 'CheckBox5',
          },
          {
            name: 'CheckBox6',
          },
          {
            name: 'CheckBox7',
          },
          {
            name: 'CheckBox8',
          },
          {
            name: 'CheckBox9',
          },
          {
            name: 'CheckBox10',
          },
          {
            name: 'CheckBox11',
          },
          {
            name: 'CheckBox12',
          },
        ],
      },
      {
        title: 'Test',
        filters: [
          {
            name: 'CheckBox1',
          },
          {
            name: 'CheckBox2',
          },
          {
            name: 'CheckBox3',
          },
          {
            name: 'CheckBox4',
          },
          {
            name: 'CheckBox5',
          },
          {
            name: 'CheckBox6',
          },
          {
            name: 'CheckBox7',
          },
          {
            name: 'CheckBox8',
          },
          {
            name: 'CheckBox9',
          },
          {
            name: 'CheckBox10',
          },
          {
            name: 'CheckBox11',
          },
          {
            name: 'CheckBox12',
          },
        ],
      },
      {
        title: 'Test',
        filters: [
          {
            name: 'CheckBox1',
          },
          {
            name: 'CheckBox2',
          },
          {
            name: 'CheckBox3',
          },
          {
            name: 'CheckBox4',
          },
          {
            name: 'CheckBox5',
          },
          {
            name: 'CheckBox6',
          },
          {
            name: 'CheckBox7',
          },
          {
            name: 'CheckBox8',
          },
          {
            name: 'CheckBox9',
          },
          {
            name: 'CheckBox10',
          },
          {
            name: 'CheckBox11',
          },
          {
            name: 'CheckBox12',
          },
        ],
      },
      {
        title: 'Test',
        filters: [
          {
            name: 'CheckBox1',
          },
          {
            name: 'CheckBox2',
          },
          {
            name: 'CheckBox3',
          },
          {
            name: 'CheckBox4',
          },
          {
            name: 'CheckBox5',
          },
          {
            name: 'CheckBox6',
          },
          {
            name: 'CheckBox7',
          },
          {
            name: 'CheckBox8',
          },
          {
            name: 'CheckBox9',
          },
          {
            name: 'CheckBox10',
          },
          {
            name: 'CheckBox11',
          },
          {
            name: 'CheckBox12',
          },
        ],
      },
    ];
    const { visible, hideModal } = this.props;
    return (
      <Fragment>
        <Modal
          isVisible={visible}
          onBackButtonPress={hideModal}
          onBackdropPress={hideModal}
          useNativeDriver
          backdropOpacity={0.5}
        >
          <View style={styles.modalContainer}>
            <View style={styles.swiper}>
              <SwipeIcon />
              <Text style={styles.swiperText}>Swipe right for more filter</Text>
            </View>
            <Carousel
              layout="default"
              ref={c => {
                this._carousel = c;
              }}
              data={fakeData}
              renderItem={this._renderItem}
              sliderWidth={Dwidth}
              sliderHeight={Dheight / 2 - 50}
              itemWidth={Dwidth - 48}
              itemHeight={Dheight / 2 - 50}
              activeSlideAlignment="start"
            />
          </View>
        </Modal>
      </Fragment>
    );
  }
}

// checkboxs
CustomCheckBoxes = ({ active, name, _filterItemPressHandler }) => {
  return (
    <TouchableOpacity
      onPress={() => _filterItemPressHandler(name)}
      style={[checkBoxStyles.checkBox, active && checkBoxStyles.checkBoxActive]}
    >
      <Text
        style={[
          checkBoxStyles.checkBoxText,
          active && checkBoxStyles.checkBoxTextActive,
        ]}
      >
        {name}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    height: '50%',
    width: '100%',
    position: 'absolute',
    bottom: 10,
  },
  // swiper
  swiper: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    width: '100%',
    backgroundColor: colors.white,
    borderRadius: 30,
    marginBottom: 8,
  },
  swiperText: {
    fontFamily: fonts.RobotoRegular,
    fontSize: 14,
    lineHeight: 16,
    color: '#707070',
    marginLeft: 8,
  },
});

const carouselStyles = StyleSheet.create({
  clearText: {
    color: '#EB626C',
    fontFamily: fonts.RobotoRegular,
    fontSize: 14,
    lineHeight: 16,
  },
  applyText: {
    color: '#379F6B',
    fontFamily: fonts.RobotoRegular,
    fontSize: 14,
    lineHeight: 16,
  },
  titleText: {
    color: '#464A4B',
    fontFamily: fonts.RobotoMedium,
    fontSize: 16,
    lineHeight: 15,
  },
});

const checkBoxStyles = StyleSheet.create({
  checkBox: {
    // minWidth: 60,
    width: '30%',
    height: 32,
    borderRadius: 20,
    backgroundColor: colors.background,
    paddingHorizontal: 14,
    paddingVertical: 8,
    marginBottom: 16,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  checkBoxActive: {
    backgroundColor: '#51AB7E',
  },
  checkBoxText: {
    fontFamily: fonts.RobotoRegular,
    fontSize: 12,
    color: '#707070',
    alignItems: 'center',
    justifyContent: 'center',
  },
  checkBoxTextActive: {
    color: colors.white,
  },
});
