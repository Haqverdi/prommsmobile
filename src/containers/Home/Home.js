/**
 * Home page container
 */

import React, { Component } from 'react';
import { TouchableOpacity, AsyncStorage } from 'react-native';
import { Container, Fab, Icon } from 'native-base';
import MyTabs from '../../components/MainTabs/Tabs';
import { colors } from '../../theme';
import MyHeader from '../../components/Header/Header';
// modals
import FilterModal from './FilterModal';
import { CancelReasonModal } from './CancelReasonModal';

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // filer modal
      modalVisible: false,
      // cancel reason modal
      cancelReasonModalVisible: false,
      cardIdForCancelReason: null,
    };
    // force update on back
    this.willFocus = this.props.navigation.addListener('willFocus', payload => {
      // this.forceUpdate();
    });
  }

  // main tabs change handle
  handleTabsChange = index => {
    // alert('index');
    return;
  };
  // onPress card go to cardinner screen
  handleCardPress = id => {
    this.props.navigation.push('CardInnerScreen', { cardId: id });
  };

  // FilterModal handle
  _showFilterModal = () => {
    this.setState({
      modalVisible: !this.state.modalVisible,
    });
  };

  // cancel reason modal hande
  _cancelReasonModalHandle = cardId => {
    this.setState({
      cancelReasonModalVisible: !this.state.cancelReasonModalVisible,
      cardIdForCancelReason: cardId,
    });
  };

  // swipe buttons hadnle
  _swipeButtonsPressHandle = (btn, cardId) => {
    switch (btn) {
      case 'accept':
        alert('Accept btn pressed');
        break;
      case 'cancel':
        this._cancelReasonModalHandle(cardId);
      default:
        break;
    }
  };

  componentDidMount = async () => {
    await AsyncStorage.clear();
  };

  render() {
    return (
      <Container style={{ flex: 1, backgroundColor: colors.main }}>
        <MyHeader
          rightButtonPressHandle={this._showFilterModal}
          navigation={this.props.navigation}
          showSettingsButton
          showSearchButton
          title="Work Orders"
        />
        <FilterModal
          visible={this.state.modalVisible}
          hideModal={this._showFilterModal}
        />
        {/* main content */}
        <MyTabs
          onTabChange={i => this.handleTabsChange(i)}
          onCardClick={id => this.handleCardPress(id)}
          // swipes press handle
          _swipeButtonsPressHandle={this._swipeButtonsPressHandle}
        />
        {/* Fab */}
        <Fab
          active={this.state.fabStatus}
          style={{ backgroundColor: colors.main, width: 56, height: 56 }}
          position="bottomRight"
        >
          <TouchableOpacity
            style={{
              width: '100%',
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}
            // go to create screen
            onPress={() => this.props.navigation.push('CreateScreen')}
          >
            <Icon
              name="plus"
              type="MaterialCommunityIcons"
              style={{ color: colors.white }}
            />
          </TouchableOpacity>
        </Fab>
        {/* Fab end */}
        <CancelReasonModal
          cancelReasonModalVisible={this.state.cancelReasonModalVisible}
          cancelModalHandle={this._cancelReasonModalHandle}
          cardId={this.state.cardIdForCancelReason}
        />
      </Container>
    );
  }
}
