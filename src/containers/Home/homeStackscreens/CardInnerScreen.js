/**
 * Tasks card inner screen
 */
import React, { PureComponent } from 'react';
import { StyleSheet } from 'react-native';
import { Container, Tabs, Tab } from 'native-base';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { colors, fonts } from '../../../theme';
import {
  Details,
  History,
  Comments,
  BottomButtons,
} from '../../../components/HomeStackComponents/index';
import { SimpleHeader } from '../../../components/Header/SimpleHeader';
import { CancelReasonModal } from '../CancelReasonModal';

// screen opens onPress at home page cards
class CardInnerScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      activeTabIndex: 0,
      cancelReasonModalVisible: false,
    };
  }
  static propTypes = {
    id: PropTypes.number,
  };

  onTabChange = index => {
    this.setState({
      activeTabIndex: index,
    });
  };

  // bottom buttons handle
  _bottomButtonTapHandle = type => {
    switch (type) {
      case 'first':
        alert('Accept');
        break;
      case 'second':
        alert('Reject');
        break;
      case 'third':
        this.setState({
          cancelReasonModalVisible: !this.state.cancelReasonModalVisible,
          cardId: this.props.cardId,
        });
        break;
      default:
        this.setState({
          cancelReasonModalVisible: false,
        });
        break;
    }
  };

  render() {
    return (
      <Container>
        {/* Header */}
        <SimpleHeader
          navigationHandle={() => this.props.navigation.popToTop(null)}
          title="Ac Works"
          hasTabs
        />
        <Tabs
          onChangeTab={({ i }) => this.onTabChange(i)}
          tabBarUnderlineStyle={{ backgroundColor: colors.main, height: 3 }}
        >
          <Tab
            heading="DETAILS"
            tabStyle={styles.tabStyle}
            activeTabStyle={styles.activeTabStyle}
            activeTextStyle={[
              styles.tabTextCommonStyle,
              styles.activeTextStyle,
            ]}
            textStyle={[styles.tabTextCommonStyle, styles.textStyle]}
          >
            <Details />
          </Tab>
          <Tab
            heading="HISTORY"
            tabStyle={styles.tabStyle}
            activeTabStyle={styles.activeTabStyle}
            activeTextStyle={[
              styles.tabTextCommonStyle,
              styles.activeTextStyle,
            ]}
            textStyle={[styles.tabTextCommonStyle, styles.textStyle]}
          >
            <History />
          </Tab>
          <Tab
            heading="COMMENTS"
            tabStyle={styles.tabStyle}
            activeTabStyle={styles.activeTabStyle}
            activeTextStyle={[
              styles.tabTextCommonStyle,
              styles.activeTextStyle,
            ]}
            textStyle={[styles.tabTextCommonStyle, styles.textStyle]}
          >
            <Comments />
          </Tab>
        </Tabs>
        {/* cancel reason modal */}
        <CancelReasonModal
          cancelReasonModalVisible={this.state.cancelReasonModalVisible}
          cancelModalHandle={this._bottomButtonTapHandle}
          cardId={this.state.cardIdForCancelReason}
        />
        {this.state.activeTabIndex !== 2 && (
          <BottomButtons
            // withTwoButtons
            buttonsPressHandle={this._bottomButtonTapHandle}
          />
        )}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  tabStyle: {
    backgroundColor: colors.white,
  },
  activeTabStyle: {
    backgroundColor: colors.white,
    color: colors.main,
  },
  activeTextStyle: {
    color: colors.main,
  },
  textStyle: {
    color: colors.black,
  },
  tabTextCommonStyle: {
    fontSize: 12,
    fontFamily: fonts.PrivaProTwo,
  },
});

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CardInnerScreen);
