/**
 * Task Create modal`s Manual page
 */

import React, { PureComponent } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types';
import { Container, Textarea, Picker } from 'native-base';
import moment from 'moment';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { colors, fonts } from '../../../../theme';
import {
  Generalinfo,
  LocationAndAsset,
  PhotoAdd,
} from '../../../../components/CreateComponents';

export default class Manual extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      selectedOrderType: 'key2',
      description: '',
      selected: 'key1',
      isDateTimePickerVisible: false,
      date: '00 : 30',
    };
  }
  static propTypes = {};

  // Work order type select handle
  _hanldeOrderTypeChange = value => {
    this.setState({
      selectedOrderType: value,
    });
  };

  // description textarea handler
  _descriptionChangeHandle = value => {
    this.setState({
      description: value,
    });
  };

  // Date time picker handle
  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

  _handleDatePicked = date => {
    this.setState({
      date: `${moment(date).format('HH : mm')}`,
    });
    this._hideDateTimePicker();
  };

  // picker handle
  _handlePickerChange = value => {
    this.setState({
      selected: value,
    });
  };

  render() {
    const { _handleImagePicker, images, _handleImageDelete } = this.props;
    return (
      <Container style={styles.container}>
        <ScrollView showsVerticalScrollIndicator={false}>
          {/* task general info */}
          <Generalinfo />
          {/* order type select */}
          <View style={styles.orderTypeContainer}>
            <Text style={styles.orderTypeText}>WO Type</Text>
            <Picker
              selectedValue={this.state.selectedOrderType}
              style={styles.orderTypePicker}
              onValueChange={itemValue =>
                this._hanldeOrderTypeChange(itemValue)
              }
              headerBackButtonText="Geri"
              iosHeader="WO type secin"
            >
              <Picker.Item label="Lorem Ipsum1" key="key1" value="key1" />
              <Picker.Item label="Lorem Ipsum2" key="key2" value="key2" />
              <Picker.Item label="Lorem Ipsum3" key="key3" value="key3" />
              <Picker.Item label="Lorem Ipsum4" key="key4" value="key4" />
              <Picker.Item label="Lorem Ipsum5" key="key5" value="key5" />
            </Picker>
          </View>
          {/* order type select end */}
          {/* Task detail */}
          <View style={styles.detailsContainer}>
            <Text style={styles.orderTypeText}>Task code</Text>
            <View style={styles.codeAndTimeContainer}>
              <View style={styles.orderDetailsTypeContainer}>
                <Picker
                  selectedValue={this.state.selected}
                  style={styles.orderTypePicker}
                  onValueChange={itemValue =>
                    this._handlePickerChange(itemValue)
                  }
                  headerBackButtonText="Geri"
                  iosHeader="Birini secin"
                >
                  <Picker.Item label="Lorem Ipsum1" key="key1" value="key1" />
                  <Picker.Item label="Lorem Ipsum2" key="key2" value="key2" />
                  <Picker.Item label="Lorem Ipsum3" key="key3" value="key3" />
                  <Picker.Item label="Lorem Ipsum4" key="key4" value="key4" />
                  <Picker.Item label="Lorem Ipsum5" key="key5" value="key5" />
                </Picker>
              </View>
              <TouchableOpacity
                onPress={() => this._showDateTimePicker()}
                style={styles.timePickerContainer}
              >
                <Text style={styles.timePickerText}>{this.state.date}</Text>
              </TouchableOpacity>
              <DateTimePicker
                isVisible={this.state.isDateTimePickerVisible}
                onConfirm={this._handleDatePicked}
                onCancel={this._hideDateTimePicker}
                mode="time"
                cancelTextIOS="Imtina"
                confirmTextIOS="Tesdiq et"
                titleIOS="Vaxti secin"
              />
            </View>
          </View>
          {/* Task detail end */}
          {/* Work order desription */}
          <View style={styles.descriptionContainer}>
            <Text style={styles.descriptionText}>Description</Text>
            <Textarea
              style={styles.descriptionInput}
              onChangeText={this._descriptionChangeHandle}
              value={this.state.description}
            />
          </View>
          {/* Work order desription end */}
          <LocationAndAsset />
          {/* Add photo */}
          <PhotoAdd
            _handleImagePicker={_handleImagePicker}
            _handleImageDelete={_handleImageDelete}
            images={images}
          />
          {/* Add photo */}
        </ScrollView>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  // manual component styles
  container: {
    backgroundColor: colors.background,
    paddingHorizontal: 16,
    paddingTop: 24,
  },
  descriptionContainer: {
    marginBottom: 16,
  },
  descriptionText: {
    marginBottom: 8,
    fontFamily: fonts.RobotoRegular,
    fontSize: 12,
    lineHeight: 16,
    color: '#868686',
    textAlign: 'left',
  },
  descriptionInput: {
    width: '100%',
    height: 80,
    backgroundColor: colors.white,
    borderRadius: 4,
    paddingLeft: 16,
    paddingTop: 16,
    paddingBottom: 16,
    paddingRight: 16,
  },
  // order type select styles
  orderTypeContainer: {
    marginBottom: 16,
  },
  orderTypePicker: {
    backgroundColor: colors.white,
    width: '100%',
    height: 40,
  },
  orderTypeText: {
    fontFamily: fonts.RobotoRegular,
    fontSize: 12,
    lineHeight: 16,
    color: '#868686',
    marginBottom: 8,
  },
  // Task details part styles
  detailsContainer: {
    marginBottom: 16,
    width: '100%',
  },
  orderDetailsTypeContainer: {
    flex: 2,
  },
  orderTypePicker: {
    backgroundColor: colors.white,
    width: '100%',
    height: 40,
  },
  orderTypeText: {
    fontFamily: fonts.RobotoRegular,
    fontSize: 12,
    lineHeight: 16,
    color: '#868686',
    marginBottom: 8,
    width: '100%',
  },
  codeAndTimeContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
  },
  timePickerContainer: {
    height: 40,
    flex: 1,
    backgroundColor: colors.white,
    marginLeft: 6,
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  timePickerText: {
    fontFamily: fonts.RobotoRegular,
    fontSize: 14,
    lineHeight: 20,
    color: '#2F343F',
  },
});
