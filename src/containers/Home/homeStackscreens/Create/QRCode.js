/**
 * Task Create modal QRCode page
 */

import React, { PureComponent } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  FlatList,
  ScrollView,
} from 'react-native';
import PropTypes from 'prop-types';
import { Container, Textarea, CheckBox, ListItem, Body } from 'native-base';
import {
  Generalinfo,
  LocationAndAsset,
} from '../../../../components/CreateComponents';
import moment from 'moment';
import { colors, fonts } from '../../../../theme/index';

export default class QRCode extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      QRCodeScanned: false,
      description: '',
      checkBoxValue: '',
    };
  }

  // show qrcode scan modal
  _showQRCodeScanModal = () => {};

  _descriptionChangeHandle = value => {
    this.setState({
      description: value,
    });
  };

  _checkBoxChangeHandle = value => {
    this.setState({
      checkBoxValue: value,
    });
  };

  static propTypes = {};

  render() {
    return (
      <Container style={styles.container}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <Generalinfo />
          {this.state.QRCodeScanned ? (
            <QRCodeScannedRender
              _descriptionChangeHandle={this._descriptionChangeHandle}
              checkBoxChange={this._checkBoxChangeHandle}
              value={this.state.description}
              checkBoxValue={this.state.checkBoxValue}
            />
          ) : (
            <QRCodeStartRender showModal={() => this._showQRCodeScanModal()} />
          )}
        </ScrollView>
      </Container>
    );
  }
}

// Qrcode NOT scanned
const QRCodeStartRender = ({ showModal }) => {
  return (
    <View style={renderStartStyles.container}>
      <TouchableOpacity onPress={() => showModal()}>
        <Image
          style={renderStartStyles.image}
          source={require('../../../../assets/img/QRCodeScan.png')}
        />
      </TouchableOpacity>
      <View style={renderStartStyles.textContainer}>
        <Text>Lorem ipsum dolor set amet,</Text>
        <Text>Lorem ipsum dolor set amet</Text>
      </View>
    </View>
  );
};

// QRCode scanned
const QRCodeScannedRender = ({
  _descriptionChangeHandle,
  value,
  checkBoxChange,
  checkBoxValue,
}) => {
  return (
    <View>
      <View style={renderStartStyles.container}>
        <Image
          style={renderStartStyles.image}
          source={require('../../../../assets/img/QRcodeScanned.png')}
        />
        <View style={styles.infoTextContainer}>
          <Text style={styles.infoName}>
            Order type: <Text style={styles.infoValue}>Air conditioner</Text>
          </Text>
          <Text style={styles.infoName}>
            Requested by: <Text style={styles.infoValue}>H.Mammad</Text>
          </Text>
          <Text style={styles.infoName}>
            Time:{' '}
            <Text style={styles.infoValue}>{moment().format('HH:mm')}</Text>
          </Text>
        </View>
      </View>
      <View style={styles.descriptionContainer}>
        <Text style={styles.descriptionText}>Description</Text>
        <Textarea
          style={styles.descriptionInput}
          onChangeText={_descriptionChangeHandle}
          value={value}
        />
      </View>
      <LocationAndAsset />
      <View>
        <Text style={styles.descriptionText}>Type</Text>
        <FlatList
          data={[
            { name: 'Xarab olub' },
            { name: 'Matoru yanib11' },
            { name: 'Matoru yanib23' },
            { name: 'Matoru yanib33' },
            { name: 'Matoru yanib44' },
          ]}
          keyExtractor={item => item.name}
          renderItem={({ item }) => {
            return (
              <ListItem>
                <CheckBox
                  onPress={() => checkBoxChange(item.name)}
                  color={colors.main}
                  checked={checkBoxValue == item.name && true}
                  style={{ marginRight: 20 }}
                />
                <Body>
                  <Text>{item.name}</Text>
                </Body>
              </ListItem>
            );
          }}
        />
      </View>
    </View>
  );
};

// qrcode not scanned styles
const renderStartStyles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  image: {
    width: 120,
    height: 120,
    marginRight: 20,
  },
  textContainer: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  text: {
    fontFamily: fonts.RobotoRegular,
    fontSize: 14,
    lineHeight: 20,
    color: '#373737',
    textAlign: 'left',
  },
});

// qrcode scanned and general styles
const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.background,
    paddingHorizontal: 16,
    paddingTop: 24,
  },
  infoTextContainer: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'space-around',
  },
  descriptionContainer: {
    marginBottom: 16,
    marginTop: 16,
  },
  descriptionText: {
    marginBottom: 8,
    fontFamily: fonts.RobotoMedium,
    fontSize: 12,
    lineHeight: 16,
    color: '#868686',
    textAlign: 'left',
  },
  descriptionInput: {
    width: '100%',
    height: 80,
    backgroundColor: colors.white,
    borderRadius: 4,
    paddingLeft: 16,
    paddingTop: 16,
    paddingBottom: 16,
    paddingRight: 16,
  },
  infoName: {
    fontFamily: fonts.RobotoBold,
    fontSize: 14,
    lineHeight: 20,
    color: colors.black,
    fontWeight: 'bold',
    maxWidth: '100%',
  },
  infoValue: {
    fontFamily: fonts.RobotoRegular,
    fontSize: 14,
    lineHeight: 16,
    color: '#373737',
  },
});

// const mapStateToProps = state => ({});

// const mapDispatchToProps = {};

// export default connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(QRCode);
