/**
 * Create page (with 2 modals)
 */

import React, { Component } from 'react';
import { Keyboard, Platform, StyleSheet, Modal } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
import {
  Container,
  Tabs,
  Tab,
  Icon,
  Header,
  Body,
  Title,
  Left,
  Right,
} from 'native-base';
import { colors, fonts } from '../../../../theme';

// modal contents
import Manual from './Manual';
import QRCode from './QRCode';

class Create extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeTab: 0, // for manual, or 1 for qrcode
      dataForSend: {
        images: [],
        woType: '',
        taskCode: '',
        time: '',
        description: '',
        asset: '',
        location: '',
        draft: false,
      },
    };
  }

  static propTypes = {};

  // navbar buttons pressed handle
  _navigationButtonPressed = buttonId => {
    if (buttonId == 'back') {
      Keyboard.dismiss();
      this.props.navigation.popToTop();
    } else {
      Keyboard.dismiss();
      this.props.navigation.popToTop();
    }
  };

  // image picker options
  options = {
    title: 'Şəkil seçin',
    quality: 0.7,
    maxHeight: 1080,
    maxWidth: 1080,
    mediaType: 'photo',
    allowsEditing: 'false',
    noData: Platform.OS == 'ios' ? true : false,
    takePhotoButtonTitle: 'Şəkil çək',
    chooseFromLibraryButtonTitle: 'Qalereyadan seç',
    cancelButtonTitle: 'Ləğv et',
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };

  // image picker
  _handleImagePicker = () => {
    ImagePicker.showImagePicker(this.options, response => {
      if (response.didCancel) {
        return;
      } else if (response.error) {
        return;
      } else {
        this.setState({
          dataForSend: {
            images: [
              ...this.state.dataForSend.images,
              {
                base64Data: 'data:image/jpeg;base64,' + response.data,
                source: response.uri.substring(7),
                size: (response.fileSize * 0.00001).toFixed(2),
                name: response.fileName,
                type: response.type,
              },
            ],
          },
        });
      }
    });
  };

  // delete image from images (on Modal -> PhotoAdd -> PhotoShowModal)
  _handleImageDelete = ImageIndex => {
    this.setState({
      dataForSend: {
        images: this.state.dataForSend.images.filter(
          (item, index) => ImageIndex != index
        ),
      },
    });
  };

  // handle tab change
  _handleTabChange = i => {
    this.setState({
      activeTab: i,
    });
  };

  render() {
    return (
      <Container>
        {/* Header */}
        <Header style={{ backgroundColor: colors.main }}>
          <Left>
            <Icon
              name="close"
              type="MaterialCommunityIcons"
              style={{ color: colors.white }}
              onPress={() => this._navigationButtonPressed('back')}
            />
          </Left>
          <Body
            style={{
              alignItems: Platform.OS === 'android' ? 'flex-end' : null,
            }}
          >
            <Title
              style={{
                color: colors.white,
                fontFamily: fonts.RobotoBold,
                fontSize: 18,
              }}
            >
              Create
            </Title>
          </Body>
          <Right>
            <Icon
              name="check"
              type="MaterialCommunityIcons"
              style={{ color: colors.white }}
              onPress={() => this._navigationButtonPressed('done')}
            />
          </Right>
        </Header>
        {/* Header end */}
        <Tabs
          // ref for tab automatic switching
          ref={c => {
            this.tabs = c;
            return;
          }}
          tabBarUnderlineStyle={{ backgroundColor: colors.main, height: 3 }}
          onChangeTab={({ i }) => this._handleTabChange(i)}
        >
          {/* Manual tab */}
          <Tab
            heading="MANUAL"
            tabStyle={styles.tabStyle}
            activeTabStyle={styles.activeTabStyle}
            activeTextStyle={[
              styles.tabTextCommonStyle,
              styles.activeTextStyle,
            ]}
            textStyle={[styles.tabTextCommonStyle, styles.textStyle]}
          >
            {/* Manual tab content */}
            <Manual
              images={this.state.dataForSend.images}
              _handleImagePicker={this._handleImagePicker}
              _handleImageDelete={this._handleImageDelete}
            />
            {/* Manual tab content */}
          </Tab>
          {/* Manual tab */}
          {/* QR Code tab */}
          <Tab
            heading="QR CODE"
            tabStyle={styles.tabStyle}
            activeTabStyle={styles.activeTabStyle}
            activeTextStyle={[
              styles.tabTextCommonStyle,
              styles.activeTextStyle,
            ]}
            textStyle={[styles.tabTextCommonStyle, styles.textStyle]}
          >
            {/* QR Code content */}
            <QRCode />
            {/* QR Code content end */}
          </Tab>
          {/* QR Code tab end */}
        </Tabs>
      </Container>
    );
  }
}

// tabs general styles
const styles = StyleSheet.create({
  tabStyle: {
    backgroundColor: colors.white,
  },
  activeTabStyle: {
    backgroundColor: colors.white,
    color: colors.main,
    fontFamily: fonts.RobotoRegular,
  },
  activeTextStyle: {
    color: colors.main,
  },
  textStyle: {
    color: colors.black,
  },
  tabTextCommonStyle: {
    fontSize: 12,
    fontFamily: fonts.RobotoRegular,
  },
});

// manual tab styles

// qrcode tab styles

const mapStateToProps = state => ({});

export default connect(
  mapStateToProps,
  {}
)(Create);
