import React, { Component } from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  StatusBar,
  StyleSheet,
  View,
} from 'react-native';
import { colors } from '../../theme';

class Login extends Component {
  constructor(props) {
    super(props);
    // login check
    this._asyncLoginCheck();
  }

  // top bar options (react navigation)
  static navigationOptions = {
    header: null,
  };

  _asyncLoginCheck = async () => {
    const userToken = await AsyncStorage.getItem('TOKEN');

    // switch to the App screen or SignIn screen
    this.props.navigation.navigate(userToken ? 'App' : 'SignIn');
  };

  render() {
    return (
      <View style={styles.container}>
        <StatusBar barStyle="default" />
        <ActivityIndicator color={colors.white} size="large" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.main,
  },
});

export default Login;
