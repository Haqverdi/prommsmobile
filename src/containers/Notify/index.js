import React, { PureComponent } from 'react';
import { Container } from 'native-base';
import MyHeader from '../../components/Header/Header';

export default class Notify extends PureComponent {
  render() {
    return (
      <Container>
        <MyHeader title="Notifications" navigation={this.props.navigation} />
      </Container>
    );
  }
}
