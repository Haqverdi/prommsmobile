import React, { PureComponent } from 'react';
import { AsyncStorage } from 'react-native';
import { Container, Content } from 'native-base';
import { colors } from '../../theme';
import {
  ProfileCard,
  QRCode,
  InfoCard,
  Popup,
} from '../../components/ProfileComponents';
import MyHeader from '../../components/Header/Header';

export default class Profile extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      // test data
      data: [
        {
          title: 'Business',
          count: 3,
        },
        {
          title: 'Orders',
          count: 8,
        },
        {
          title: 'Users',
          count: 12,
        },
      ],
      sValue: false,
      info: [
        {
          title: 'Bölmə',
          info: 'Marketing',
        },
        {
          title: 'Otaq',
          info: '#156',
        },
        {
          title: 'Vəzifə',
          info: 'Dizayner',
        },
      ],
      modalVisible: false,
      modalTitle: null,
    };
    this.getQRCodeStatus();
  }

  // bottom tabs changes handle
  handleTabBarChange = tab => {
    goToBottomTab(tab);
  };

  // qrcode switcher handle
  handleToggleSwitch = async value => {
    try {
      this.setState({
        sValue: value,
      });
      value
        ? await AsyncStorage.setItem('QRCode', `${true}`)
        : await AsyncStorage.removeItem('QRCode');
    } catch (error) {
      return;
    }
  };

  // check qrcode switcher checked or not and update state
  getQRCodeStatus = async () => {
    try {
      const status = await AsyncStorage.getItem('QRCode');
      if (status != null) {
        this.setState({
          sValue: true,
        });
      }
    } catch (error) {
      return;
    }
  };

  // on (businness, rdesrs, etc.) press show info modal
  handleModalClick = data => {
    this.setState({
      modalVisible: !this.state.modalVisible,
      modalTitle: data,
    });
  };
  //close opened modal
  handleModalClose = () => {
    this.setState({
      modalVisible: false,
    });
  };

  render() {
    // test data
    const companies = [
      {
        title: 'Red Room',
        logo:
          'https://www.logogarden.com/wp-content/uploads/lg-logo-samples/Construction-Handyman-Logo-1.png',
      },
      {
        title: 'Harpie',
        logo:
          'https://www.logogarden.com/wp-content/uploads/lg-logo-samples/Construction-Handyman-Logo-2.png',
      },
      {
        title: 'Basis',
        logo:
          'https://www.logogarden.com/wp-content/uploads/lg-logo-samples/Construction-Handyman-Logo-4.png',
      },
    ];
    const users = [
      {
        title: 'Black Room',
        logo:
          'https://www.logogarden.com/wp-content/uploads/lg-logo-samples/Construction-Handyman-Logo-1.png',
      },
      {
        title: 'Harpie',
        logo:
          'https://www.logogarden.com/wp-content/uploads/lg-logo-samples/Construction-Handyman-Logo-2.png',
      },
      {
        title: 'Basis',
        logo:
          'https://www.logogarden.com/wp-content/uploads/lg-logo-samples/Construction-Handyman-Logo-4.png',
      },
      {
        title: 'Red Room',
        logo:
          'https://www.logogarden.com/wp-content/uploads/lg-logo-samples/Construction-Handyman-Logo-1.png',
      },
      {
        title: 'Harpie',
        logo:
          'https://www.logogarden.com/wp-content/uploads/lg-logo-samples/Construction-Handyman-Logo-2.png',
      },
      {
        title: 'Basis',
        logo:
          'https://www.logogarden.com/wp-content/uploads/lg-logo-samples/Construction-Handyman-Logo-4.png',
      },
      {
        title: 'Basis',
        logo:
          'https://www.logogarden.com/wp-content/uploads/lg-logo-samples/Construction-Handyman-Logo-4.png',
      },
      {
        title: 'Red Room',
        logo:
          'https://www.logogarden.com/wp-content/uploads/lg-logo-samples/Construction-Handyman-Logo-1.png',
      },
      {
        title: 'Harpie',
        logo:
          'https://www.logogarden.com/wp-content/uploads/lg-logo-samples/Construction-Handyman-Logo-2.png',
      },
      {
        title: 'Basis',
        logo:
          'https://www.logogarden.com/wp-content/uploads/lg-logo-samples/Construction-Handyman-Logo-4.png',
      },
    ];
    const orders = [
      {
        title: 'Basis',
        count: 12,
        type: 'supervised',
        logo:
          'https://www.logogarden.com/wp-content/uploads/lg-logo-samples/Construction-Handyman-Logo-4.png',
      },
      {
        title: 'Red Room',
        count: 12,
        type: 'paused',
        logo:
          'https://www.logogarden.com/wp-content/uploads/lg-logo-samples/Construction-Handyman-Logo-1.png',
      },
      {
        title: 'Harpie',
        count: 12,
        type: 'rejected',
        logo:
          'https://www.logogarden.com/wp-content/uploads/lg-logo-samples/Construction-Handyman-Logo-2.png',
      },
      {
        title: 'Basis',
        count: 12,
        type: 'expired',
        logo:
          'https://www.logogarden.com/wp-content/uploads/lg-logo-samples/Construction-Handyman-Logo-4.png',
      },
    ];
    return (
      <Container style={{ backgroundColor: colors.background }}>
        <MyHeader
          navigation={this.props.navigation}
          title="Profile"
          showSettingsButton
          rightButtonPressHandle={() =>
            this.props.navigation.push('ProfileEdit')
          }
        />
        <Content
          showsVerticalScrollIndicator={false}
          padder
          contentContainerStyle={{
            marginHorizontal: 16,
            marginTop: 8,
            paddingHorizontal: 0,
            paddingTop: 0,
          }}
        >
          {/* profile info */}
          <ProfileCard
            name="Profile name"
            data={this.state.data}
            onPressed={title => this.handleModalClick(title)}
          />
          <Popup
            modalVisible={this.state.modalVisible}
            modalTitle={this.state.modalTitle}
            modalData={
              this.state.modalTitle == 'Business'
                ? companies
                : this.state.modalTitle == 'Orders'
                ? orders
                : users
            }
            handleModalClick={() => this.handleModalClose()}
          />
          {/* QRcode component */}
          <QRCode
            sValue={this.state.sValue}
            toggleSwitch={this.handleToggleSwitch}
          />
          {/* render avaliable info list */}
          {this.state.info.map(item => (
            <InfoCard key={item.title} {...item} />
          ))}
        </Content>
      </Container>
    );
  }
}
