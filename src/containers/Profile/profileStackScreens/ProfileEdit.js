import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Container } from 'native-base';
import { SimpleHeader } from '../../../components/Header/SimpleHeader';

class ProfileEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Container>
        <SimpleHeader
          title="Edit Profile"
          navigationHandle={() => this.props.navigation.popToTop()}
          showRightButton
          rightButtonPressHandle={() => this.props.navigation.popToTop()}
        />
      </Container>
    );
  }
}

export default ProfileEdit;
