/**
 * Sidebar component
 */

import React, { PureComponent } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Platform,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import { Container, Button, Header, Thumbnail, Left } from 'native-base';
import {
  colors,
  fonts,
  HomeIcon,
  CompanyIcon,
  NotifyIcon,
  ProfileIcon,
} from '../../theme';

// platform
const platfrom = Platform.OS;

class SideBar extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      activeScreen: 'Home',
    };
  }

  // go to selected screen
  goToSelectedScreen = screen => {
    // `edit profile` button pressed, go to editProfile screen and set Profile as active
    if (screen === 'ProfileEdit') {
      this.props.navigation.navigate(screen);
      this.setState({
        activeScreen: 'Profile',
      });
    } else {
      this.props.navigation.navigate(screen);
      this.setState({
        activeScreen: screen,
      });
    }
  };

  render() {
    const { activeScreen } = this.state;
    return (
      <Container style={sideBarStyles.container}>
        <Header style={sideBarStyles.header}>
          {/* background image */}
          <ImageBackground
            resizeMode="cover"
            style={{ width: '100%' }}
            source={require('../../assets/img/sideBarBackground.png')}
          >
            <Left style={sideBarStyles.headerLeft}>
              {/* thumb image */}
              <Thumbnail
                large
                source={require('../../assets/img/profile.jpg')}
                style={{ marginBottom: 10 }}
              />
              {/* name */}
              <Text style={sideBarStyles.headerNameText}>Profile Name</Text>
              {/* edit */}
              <TouchableOpacity
                onPress={() => this.goToSelectedScreen('ProfileEdit')}
                // onPress={() => this.props.navigation.navigate('ProfileEdit')}
              >
                <Text style={sideBarStyles.headerEditText}>Edit profile</Text>
              </TouchableOpacity>
            </Left>
          </ImageBackground>
        </Header>
        {/* Header end */}
        {/* Content start */}
        <View style={sideBarStyles.menuContainer}>
          {/* Home */}
          <Button
            transparent
            full
            style={sideBarStyles.menuButton}
            onPress={
              activeScreen == 'Home'
                ? null
                : () => this.goToSelectedScreen('Home')
            }
          >
            <HomeIcon active={activeScreen == 'Home' ? true : false} />
            <Text
              style={[
                sideBarStyles.menuText,
                activeScreen == 'Home' && sideBarStyles.menuTextActive,
              ]}
            >
              Home
            </Text>
          </Button>
          {/* Home end */}
          {/* Company */}
          <Button
            transparent
            full
            style={sideBarStyles.menuButton}
            onPress={
              activeScreen == 'Company'
                ? null
                : () => this.goToSelectedScreen('Company')
            }
          >
            <CompanyIcon active={activeScreen == 'Company' ? true : false} />
            <Text
              style={[
                sideBarStyles.menuText,
                activeScreen == 'Company' && sideBarStyles.menuTextActive,
              ]}
            >
              Company
            </Text>
          </Button>
          {/* Company end */}
          {/* Notify */}
          <Button
            disabled
            transparent
            full
            style={sideBarStyles.menuButton}
            onPress={
              activeScreen == 'Notify'
                ? null
                : () => this.goToSelectedScreen('Notify')
            }
          >
            <NotifyIcon active={activeScreen == 'Notify' ? true : false} />
            <Text
              style={[
                sideBarStyles.menuText,
                activeScreen == 'Notify' && sideBarStyles.menuTextActive,
              ]}
            >
              Notification
            </Text>
          </Button>
          {/* Notify end */}
          {/* Profile */}
          <Button
            transparent
            full
            style={sideBarStyles.menuButton}
            onPress={
              activeScreen == 'Profile'
                ? null
                : () => this.goToSelectedScreen('Profile')
            }
          >
            <ProfileIcon active={activeScreen == 'Profile' ? true : false} />
            <Text
              style={[
                sideBarStyles.menuText,
                activeScreen == 'Profile' && sideBarStyles.menuTextActive,
              ]}
            >
              Profile
            </Text>
          </Button>
          {/* Profile end */}
        </View>
        {/* Content end */}
      </Container>
    );
  }
}

const sideBarStyles = StyleSheet.create({
  container: {
    backgroundColor: colors.sideBarBackground,
    flex: 1,
  },
  header: {
    height: platfrom == 'ios' ? 180 : 164,
    backgroundColor: colors.main,
    justifyContent: 'flex-start',
    paddingLeft: 0,
    paddingRight: 0,
    paddingBottom: 0,
  },
  headerLeft: {
    justifyContent: 'flex-start',
    paddingLeft: 10,
    minWidth: '100%',
    paddingTop: 16,
  },
  headerNameText: {
    fontSize: 14,
    lineHeight: 24,
    fontFamily: fonts.RobotoMedium,
    color: colors.white,
  },
  headerEditText: {
    fontSize: 13,
    lineHeight: 20,
    fontFamily: fonts.RobotoRegular,
    color: colors.white,
    opacity: 0.8,
  },
  menuContainer: {
    paddingLeft: 16,
    paddingTop: 8,
  },
  menuButton: {
    justifyContent: 'flex-start',
  },
  menuText: {
    marginLeft: 32,
    fontFamily: fonts.RobotoBold,
    fontSize: 16,
    lineHeight: 24,
    color: colors.muttedTextColor,
  },
  menuTextActive: {
    color: colors.black,
  },
});

export default SideBar;
