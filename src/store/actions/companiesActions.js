import * as types from '../types/companiesTypes';
import axios from 'axios';
import { call, put, takeLatest, all } from 'redux-saga/effects';
import { AsyncStorage } from 'react-native';

// sample data
const companies = [
  {
    id: 1,
    selected: false,
    title: 'Red Roomm',
    logo:
      'https://www.logogarden.com/wp-content/uploads/lg-logo-samples/Construction-Handyman-Logo-1.png',
  },
  {
    id: 2,
    selected: false,
    title: 'Harpiee',
    logo:
      'https://www.logogarden.com/wp-content/uploads/lg-logo-samples/Construction-Handyman-Logo-2.png',
  },
  {
    id: 3,
    selected: false,
    title: 'Basiss',
    logo:
      'https://www.logogarden.com/wp-content/uploads/lg-logo-samples/Construction-Handyman-Logo-4.png',
  },
];

// utils
const _writeToAsycnStorage = async data => {
  try {
    await AsyncStorage.setItem('companies', JSON.stringify(data));
  } catch (error) {
    return error;
  }
};

const _readFromAsyncStorage = async () => {
  try {
    const data = await AsyncStorage.getItem('companies');
    if (data !== null) {
      return JSON.parse(data);
    }
  } catch (error) {
    return error;
  }
};

// change selected
const changeSelectedCompany = (data, id) =>
  data.map(item =>
    item.id == id ? { ...item, selected: true } : { ...item, selected: false }
  );

// dispatch functions
export const getCompanies = () => ({ type: types.GET_COMPANIES });

export const changePrimary = selectedId => ({
  type: types.PRIMARY_CHANGE,
  selectedId,
});

//saga functions
function* getCompaniesWorker() {
  try {
    yield put({ type: types.GET_COMPANIES_LOADING });
    // fetch companies list
    const companiesFromStorage = yield _readFromAsyncStorage();
    yield put({
      type: types.GET_COMPANIES_DONE,
      payload: companiesFromStorage ? companiesFromStorage : companies,
    });
  } catch (error) {
    yield put({ type: types.GET_COMPANIES_ERROR });
  }
}

function* changePrimaryWorker({ selectedId }) {
  try {
    yield put({ type: types.PRIMARY_CHANGE_LOADING });

    const changedData = yield call(
      changeSelectedCompany,
      companies,
      selectedId
    );
    yield put({ type: types.PRIMARY_CHANGE_DONE, payload: changedData });
    yield _writeToAsycnStorage(changedData);
  } catch (error) {
    yield put({ type: types.PRIMARY_CHANGE_ERROR });
  }
}

// watcher
export function* companyWorkersWatcher() {
  yield all([
    takeLatest(types.GET_COMPANIES, getCompaniesWorker),
    takeLatest(types.PRIMARY_CHANGE, changePrimaryWorker),
  ]);
}
