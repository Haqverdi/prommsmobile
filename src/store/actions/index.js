/**
 * Actions root file, export all actions
 */

import { getCompanies, changePrimary } from './companiesActions';
import { login, readTokenFromAsyncStorage } from './loginActions';

export { getCompanies, changePrimary, login, readTokenFromAsyncStorage };
