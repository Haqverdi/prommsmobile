import { loginTypes } from '../types/loginTypes';
import axios from 'axios';
import qs from 'qs';
import { call, put, takeLatest, all } from 'redux-saga/effects';
import { AsyncStorage } from 'react-native';

// config
const URL = 'http://promms.tk/signIn';

// async storage
const writeTokenToAsyncStorage = async token => {
  try {
    await AsyncStorage.setItem('TOKEN', token);
  } catch (error) {
    return error;
  }
};

export const readTokenFromAsyncStorage = () => {
  return new Promise(async (resolve, reject) => {
    try {
      const token = await AsyncStorage.getItem('TOKEN');
      if (token !== null) {
        resolve(token);
      } else {
        resolve(false);
      }
    } catch (error) {
      reject(false);
    }
  });
};

// dispatch funcs
export const login = (
  _username = 'sahib777.5@mail.ru',
  _password = '123123'
) => ({
  type: loginTypes.LOGIN,
  _username,
  _password,
});

// export const logout = () => ({
//   type: loginTypes.LOGOUT
// })

// sagas
function* loginWorker({ _username, _password }) {
  try {
    // loading
    yield put({ type: loginTypes.LOGIN_LOADING });
    // api call
    const { data } = yield call(
      axios.post,
      URL,
      qs.stringify({ _username, _password }),
      {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      }
    );
    // done
    if (data.success) {
      yield put({ type: loginTypes.LOGIN_DONE, payload: data });
      yield writeTokenToAsyncStorage(data.token);
    } else {
      yield put({ type: loginTypes.LOGIN_ERROR, payload: data });
    }
  } catch (error) {
    yield put({ type: loginTypes.LOGIN_ERROR, payload: error });
  }
}

// saga watcher
export function* loginWatcher() {
  yield all([takeLatest(loginTypes.LOGIN, loginWorker)]);
}
