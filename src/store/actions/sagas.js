/**
 * Saga Watchers root file, export all saga watchers
 */
import { loginWatcher } from './loginActions';
import { companyWorkersWatcher } from './companiesActions';

export { loginWatcher, companyWorkersWatcher };
