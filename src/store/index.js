import { createStore, combineReducers, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';

// reducers
import { loginReducer, companiesReducer } from './reducers';

// root saga
import { rootSaga } from './saga';

// saga middleware
const sagaMiddleware = createSagaMiddleware();

// reducerS
const rootReducer = combineReducers({
  loginReducer,
  companiesReducer,
});

// creating store
const store = createStore(rootReducer, applyMiddleware(sagaMiddleware));

// run saga root watcher
sagaMiddleware.run(rootSaga);

export default store;
