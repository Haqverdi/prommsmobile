import * as types from '../types/companiesTypes';

const initialState = {
  companies: [],
  loading: false,
  error: null,
};

export const companiesReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_COMPANIES_LOADING:
      return { ...state, loading: true };
    case types.GET_COMPANIES_ERROR:
      return { ...state, error: true, loading: false };
    case types.GET_COMPANIES_DONE:
      return {
        companies: [...action.payload],
        loading: false,
        error: null,
      };
    case types.PRIMARY_CHANGE_LOADING:
      return { ...state, loading: true };
    case types.PRIMARY_CHANGE_ERROR:
      return { ...state, error: true, loading: false };
    case types.PRIMARY_CHANGE_DONE:
      return { companies: [...action.payload], loading: false, error: null };
    default:
      return state;
  }
};
