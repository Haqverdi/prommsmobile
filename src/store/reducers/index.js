import { loginReducer } from './loginReducer';
import { companiesReducer } from './companiesReducer';

export { loginReducer, companiesReducer };
