import { loginTypes } from '../types/loginTypes';

const initialState = {
  data: {},
  loading: false,
  error: null,
};

export const loginReducer = (state = initialState, action) => {
  switch (action.type) {
    case loginTypes.LOGIN_LOADING:
      return {
        ...state,
        loading: true,
      };
    case loginTypes.LOGIN_ERROR:
      return {
        ...state,
        error: true,
        loading: false,
      };
    case loginTypes.LOGIN_DONE:
      return {
        data: { ...action.payload },
        loading: false,
        error: null,
      };
    default:
      return state;
  }
};
