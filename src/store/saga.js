import { fork, all } from 'redux-saga/effects';

// sagas
import { loginWatcher, companyWorkersWatcher } from './actions/sagas';

export function* rootSaga() {
  yield all([fork(loginWatcher), fork(companyWorkersWatcher)]);
}
