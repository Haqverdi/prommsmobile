/**
 * Auth store, hadnling auth, registration and logout
 */
import {
  decorate,
  observable,
  configure,
  action,
  runInAction,
  computed,
} from 'mobx';
import { AsyncStorage } from 'react-native';
import axios from 'axios';
import qs from 'qs';
configure({ enforceActions: 'observed' });

const LOGIN_URL = 'http://promms.tk/v2/login';
const REGISTER_URL = 'http://promms.tk/v2/register';

class AuthStore {
  // initial state
  logged = false;
  loading = false;
  error = null;
  data = {};

  /* actions */
  // takes email and password and attempt to login, write content to data
  login = async (email, password) => {
    try {
      this.loading = true;
      this.error = null;
      const { data } = await axios.post(
        LOGIN_URL,
        qs.stringify({ email, password }),
        {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          },
        }
      );
      runInAction(() => {
        if (data.success) {
          this.loading = false;
          this.error = null;
          this.data = { ...data };
          this.writeTokenToAsyncStorage(data.token);
          this.logged = true;
        } else {
          this.loading = false;
          this.error = { ...data };
          this.data = {};
        }
      });
    } catch (error) {
      runInAction(() => {
        this.error = error;
        this.loading = false;
      });
    }
  };
  get loadingStatus() {
    return this.loading;
  }
  get loggedStatus() {
    return this.logged;
  }
  // register, takes email, password, passwordRepeat
  //and attempt to register, save token and etc info to data object
  async register(email, password, passwordRepeat) {
    try {
      this.loading = true;
      this.error = null;

      const { data } = await axios.post(
        REGISTER_URL,
        qs.stringify({
          email,
          password,
          password2: passwordRepeat,
        }),
        {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          },
        }
      );
      runInAction(() => {
        if (data.success) {
          this.loading = false;
          this.error = null;
          this.data = { ...data };
          this.writeTokenToAsyncStorage(data.token);
          this.logged = true;
        } else {
          this.loading = false;
          this.error = { ...data };
          this.data = {};
        }
      });
    } catch (error) {
      runInAction(() => {
        this.error = error;
        this.loading = false;
      });
    }
  }
  // save token to async store
  writeTokenToAsyncStorage = async token => {
    await AsyncStorage.setItem('TOKEN', token);
  };
}

export default decorate(AuthStore, {
  logged: observable,
  loading: observable,
  error: observable,
  data: observable,
  login: action,
  register: action,
  loadingStatus: computed,
  loggedStatus: computed,
});
