/**
 * stores root file, all stores initialised and exported here
 */

// import stores
import AuthStore from './AuthStore';

/* if need root store uncomment below */
// class RootStore {
//   constructor() {
//     this.storeName = new StoreName(this);
//     // ...
//   }
// }
// const rootStore = new RootStore();
/* if need root store uncomment upper */

export const stores = {
  AuthStore: new AuthStore(),
};
