import React from 'react';
import { ActivityIndicator } from 'react-native';
import Dialog, { DialogContent } from 'react-native-popup-dialog';
import { colors } from '../theme/index';

export default function Loading({ visible }) {
  return (
    <Dialog
      visible={visible}
      dialogStyle={{
        // borderRadius: 4,
        backgroundColor: 'transparent',
      }}
    >
      <DialogContent
      // style={{
      //   paddingBottom: 8,
      //   paddingTop: 8,
      // }}
      >
        {/* <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            height: 48,
          }}
        >
          <Text style={{ fontSize: 16, marginRight: 8 }}>Yüklənir</Text>
        </View> */}
        <ActivityIndicator color={colors.white} size="large" />
      </DialogContent>
    </Dialog>
  );
}
