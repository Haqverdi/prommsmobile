/**
 * Input validations
 */

// form validation
export const emailVerification = value => {
  const emailError = !/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/g.test(
    value
  )
    ? false
    : true;
  return emailError;
};

export const passwordVerification = (value, min = 6, max = 1000) => {
  const passwordError =
    value.length >= min && value.length <= max ? true : false;
  return passwordError;
};
