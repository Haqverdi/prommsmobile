/**
 * Read token from async storage util
 */
import { AsyncStorage } from 'react-native';

// read token from async store
export const readTokenFromAsyncStorage = () => {
  return new Promise(async (resolve, reject) => {
    try {
      const token = await AsyncStorage.getItem('TOKEN');
      if (token !== null) {
        resolve(token);
      } else {
        resolve(false);
      }
    } catch (error) {
      reject(false);
    }
  });
};
